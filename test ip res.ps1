﻿Import-Module -Name Scripts\Initialize-PowerCLIEnvironment.ps1
Import-Module 'ActiveDirectory'
Connect-VIServer gklab-157-005

$linux=0 
$windows=0
$List = @()
$Akt=0
$dir=""
$ListIP = @()
$Pth = split-path -parent $MyInvocation.MyCommand.Definition
Set-Location -Path $Pth

$List=@()
[System.Collections.ArrayList]$Windows_list = $List
[System.Collections.ArrayList]$Linux_list = $List
[System.Collections.ArrayList]$List_summary = $List
#$Windows_list=@()
#$Linux_list=@()
#$list_summary = @() 


Function IP_List() {
[System.Collections.ArrayList]$ListIP = @()
$ip=''
$x = 1
 
while ($x -ne 0) {
# -or $x -ne 2 -or $x -ne 3) {
Write-host ("Networks: ")
Write-Host ("1. 156 (10.237.156.0/23) - native - for infrastructure")
Write-Host ("2. 678 (10.91.78.0/23)   - Lab-Sustaining - vlan 678 ")
Write-Host ("3. 444 (10.91.224.0/23)  - Lab-PT - vlan 444")
Write-Host ("4. 1554 (10.91.254.0/23)  - Lab-PT - vlan 1554")

$z = Read-Host("Select Network: ")
if (($z -eq 1) -or ($z -eq 2) -or ($z -eq 3) -or ($z -eq 4) ) { $x=0 }
} 


if ($z -eq 1) { 
$pref_a='10.237.156.' 
$pref_b='10.237.157.' }
if ($z -eq 2) {
$pref_a= '10.91.78.'
$pref_b='10.91.79.' }
if ($z -eq 3) {
$pref_a='10.91.224.'
$pref_b='10.91.225.' }
if ($z -eq 4) {
$pref_a='10.91.254.'
$pref_b='10.91.255.' }



$i=6
while ($i -ne 255) { $ip=$pref_a+$i;$i++;$ListIP += $ip}
$i=0
while ($i -ne 254) { $ip=$pref_b+$i;$i++;$ListIP += $ip}

return $ListIP 
}


#$id_ip = 10.91.78.234
#10.237.156.34

function Host_name([string]$id_ip) {

#$id_ip = '10.91.78.1'

#$id_ip = '10.237.156.113'

$test2=$id_ip.Substring(0,7)


if ($test2 -eq '10.91.2'){
$g3=$id_ip.substring(6,3)
$g4=$id_ip.substring(10)}


if ($test2 -eq '10.91.7'){
$g3=$id_ip.substring(6,2)
$g4=$id_ip.substring(9)}

if ($test2 -eq '10.237.'){
$g3=$id_ip.substring(7,3)
$g4=$id_ip.substring(11)}

$g0='gklab'
if ($g4.Length -eq 1) {$g4 = '00{0}' -f $g4}
if ($g4.Length -eq 2) {$g4 = '0{0}' -f $g4}
$VMname='{0}-{1}-{2}' -f $g0,$g3,$g4

return $VMname
Write-Host $VMname
}

Function MacAddress([string]$id_ip)
{

$test2=$id_ip.Substring(0,7)
$id_ip2 = $id_ip.replace('.','')

if ($test2 -eq '10.91.2'){
    if ($id_ip2.Length -eq 8){
        $m1='2{0}' -f $id_ip2.substring(5,1)
        $m2='{0}0' -f $id_ip2.substring(6,1)
        $m3='0{0}' -f $id_ip2.substring(7,1) }
    if ($id_ip2.Length -eq 9){
        $m1='2{0}' -f $id_ip2.substring(5,1)
        $m2='{0}0' -f $id_ip2.substring(6,1)
        $m3='{0}{1}' -f $id_ip2.substring(7,1), $id_ip2.substring(8,1) }
    if ($id_ip2.Length -eq 10){
        $m1='2{0}' -f $id_ip2.substring(5,1)
        $m2='{0}{1}' -f $id_ip2.substring(6,1), $id_ip2.substring(7,1)
        $m3='{0}{1}' -f $id_ip2.substring(8,1), $id_ip2.substring(9,1) }
}

if ($test2 -eq '10.91.7'){
    if ($id_ip2.Length -eq 7){
        $m1='0{0}' -f $id_ip2.substring(4,1)
        $m2='{0}0' -f $id_ip2.substring(5,1)
        $m3='0{0}' -f $id_ip2.substring(6,1) }
    if ($id_ip2.Length -eq 8){
        $m1='0{0}' -f $id_ip2.substring(4,1)
        $m2='{0}0' -f $id_ip2.substring(5,1)
        $m3='{0}{1}' -f $id_ip2.substring(6,1), $id_ip2.substring(7,1) }
    if ($id_ip2.Length -eq 9){
        $m1='0{0}' -f $id_ip2.substring(4,1)
        $m2='{0}{1}' -f $id_ip2.substring(5,1), $id_ip2.substring(6,1)
        $m3='{0}{1}' -f $id_ip2.substring(7,1), $id_ip2.substring(8,1) }
}

if ($test2 -eq '10.237.'){
    if ($id_ip2.Length -eq 9){
        $m1='1{0}' -f $id_ip2.substring(6,1)
        $m2='{0}0' -f $id_ip2.substring(7,1)
        $m3='0{0}' -f $id_ip2.substring(8,1) }
    if ($id_ip2.Length -eq 10){
        $m1='1{0}' -f $id_ip2.substring(6,1)
        $m2='{0}0' -f $id_ip2.substring(7,1)
        $m3='{0}{1}' -f $id_ip2.substring(8,1), $id_ip2.substring(9,1) }
    if ($id_ip2.Length -eq 11){
        $m1='1{0}' -f $id_ip2.substring(6,1)
        $m2='{0}{1}' -f $id_ip2.substring(7,1), $id_ip2.substring(8,1)
        $m3='{0}{1}' -f $id_ip2.substring(9,1), $id_ip2.substring(10,1) }
}

$m0='00:50:56'
$MacAdd='{0}:{1}:{2}:{3}' -f $m0,$m1,$m2,$m3
#Write-Host $MacAdd
return $MacAdd

}



$a = IP_List
#write-host $a
Start-Sleep 1
write-host #########################################################################################################################################################
"IP,Mac,Name,Description" > C:\Temp\ddi_list.txt 
"" > C:\Temp\dns_list.txt 
foreach ($vm in $a){ 
    $test = Test-Connection $vm -Count 1 -quiet 
    if (!$test) {
        #write-host $vm
        $ho=Host_name($vm)
        $mac_a= MacAddress($vm) 
        $name=$ho+'.igk.intel.com'
        write-host $vm,`t,$ho,`t,$mac_a,`t,$name
        #Write-output $vm,' ',$ho,' ',$mac_a >> C:\Temp\ddi_list.txt 
        Add-Content -Path  C:\Temp\ddi_list.txt -NoNewline  -Value $vm,',',$mac_a,',',$ho,',',$name
        Add-Content -Path  C:\Temp\ddi_list.txt -Value ''
        ######################################################
        Add-Content -Path  C:\Temp\dns_list.txt -NoNewline  -Value 'Add, ',$name,', 7200, A, ',$vm
        Add-Content -Path  C:\Temp\dns_list.txt -Value ''
        #New-ADComputer -Name $ho -SAMAccountName $ho -ManagedBy "ITP Process Team Admins" -Description 'owner:ITP Process Team Admins' -DNSHostName $ho.ger.corp.intel.com -Path "OU=Web,OU=Servers,OU=IGK,OU=Customer Labs,OU=Engineering Computing,OU=Resources,DC=ger,DC=corp,DC=intel,DC=com" -Enabled $True -Location "IGK-GDANSK"


        #$ho1=Get-VM $ho -ErrorAction SilentlyContinue 
        #if ($ho1 -eq $null ){write-host 'free'} else { Write-Host $ho1.Name}
        }  
}





