﻿Import-Module  -Name sqlps



#backup MS-CLUSTER

$servers = @("GKPT01SQL\CLUSTER","GKLAB-156-080\Default")

$storages = @("\\10.237.156.250\databasebackup\","\\gksfiler\nfs\igk\disks\igk_pt_backups\")

$retention = 15

###########################################################################


foreach ($server in $servers){
    $List_ret=@()
    foreach ($storage in $storages){
        
        #$stor=$storage+$server
        Set-Location "SQLSERVER:\SQL\$server\Databases" 
        New-PSDrive -Name "w" -PSProvider "FileSystem" -Root "$storage" 
        if (!(Test-Path w:\$server)){ New-Item w:\$server -Force -Type directory | Out-Null }
        #Set-Location "SQLSERVER:\SQL\$server\Databases" 
        ForEach($database in (Get-ChildItem)) {
            $dbName = $database.Name
            $now = Get-Date
            $day = $now.DayOfYear
            $time = $now.TimeOfDay
            $time=$time -Replace(":",".")
            Backup-SqlDatabase -CompressionOption On -Database $dbName -BackupFile "d:\$server\$dbName.$day.$time.bak"
            #$File_name='{0}.{1}.{2}.bak' -f $dbName,$day,$time
            #$List_bkp += $File_name
            $now_ret =  (Get-Date).AddDays(-$retention)
            $day_ret= $now_ret.DayOfYear
            $File_name_ret='{0}.{1}.bak' -f $dbName,$day_ret
            $List_ret += $File_name_ret
        }
        Set-Location -Path C:\
        foreach ($File_name in $List_ret){Remove-Item  d:\$server\$File_name* -Force -Verbose }
        Remove-PSDrive -LiteralName "w" -Confirm:$false
    }
}