﻿

#############################################################################################################
$KeyFile = "c:\passwordstore\AES.key"
$key = Get-Content $KeyFile
#############################################################################################################
$Adm = "VSPHERE.LOCAL\Administrator"
$Adm_Vcenter = "C:\passwordstore\Vcenter_Adm_Pass.txt"
$Vcenter_Cred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $Adm, (Get-Content $Adm_Vcenter | ConvertTo-SecureString -Key $key)
#############################################################################################################


#Connect-VIServer gklab-156-255 -Credential $Vcenter_Cred
#Start-Sleep 10
#Disconnect-VIServer gklab-156-255 -Confirm:$false



$usr=[Security.Principal.WindowsIdentity]::getcurrent().name

if (!(Connect-VIServer gklab-156-255)) { Connect-VIServer gklab-156-255 -SaveCredentials -User $usr }


Start-sleep 10

Disconnect-VIServer gklab-156-255 -Confirm:$false
