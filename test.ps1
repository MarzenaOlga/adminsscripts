﻿$lista = @(
'gklab-156-017',
'gklab-156-015',
'gklab-156-188',
'gklab-156-136',
'gklab-156-136',
'gklab-156-015',
'gklab-156-015',
'gklab-156-132',
'gklab-156-132',
'gklab-156-135',
'gklab-156-135',
'gklab-156-134',
'gklab-156-134',
'gklab-156-202',
'gklab-79-223',
'gklab-79-223',
'gklab-156-138',
'gklab-156-138',
'gklab-156-031',
'gklab-156-046',
'gklab-156-131',
'gklab-156-131',
'gklab-79-224',
'gklab-79-224',
'gklab-156-018',
'gklab-156-043',
'gklab-156-018',
'gklab-156-035',
'gklab-156-042',
'gklab-156-137',
'gklab-156-137',
'gklab-156-047',
'gklab-156-017',
'gklab-156-017',
'gklab-156-037'
)
foreach ($server in $lista)
{
   $version = Invoke-Command -Computer $server -ScriptBlock {
      (Get-WmiObject Win32_SoftwareElement | ? { $_.name -eq "system.net.dll_x86" }).Version
   }
   Write-Output "$server is using .Net Framework version $version"
}