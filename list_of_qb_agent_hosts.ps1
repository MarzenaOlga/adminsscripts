﻿#Skrypt tworzy na pulpicie dwie listy VM hostów: z i bez zainstalowanego agenta QuickBuilda

Import-Module -Name Scripts\Initialize-PowerCLIEnvironment.ps1
Connect-VIServer gklab-157-005

$hosts = Get-VM | Where-Object {$_.PowerState -EQ "PoweredOn" -and $_.Name -Like "gklab*"}
$hostsWQB = @()
$hostsWoQB = @()
ForEach ($hostName in $hosts) {
    if (Test-NetConnection -ComputerName $hostName.Name -Port 8841 -InformationLevel Quiet) {
        $hostsWQB += $hostName
    } else {
        $hostsWoQB += $hostName
    }
}

$hostsWQB | Select-Object Name, @{Name="ProvisionedSpaceGB";Expression={"{0:N0}" -f ($_.ProvisionedSpaceGB)}} | Out-File "~\Desktop\with-quickbuild.txt"
$hostsWoQB | Select-Object Name, @{Name="ProvisionedSpaceGB";Expression={"{0:N0}" -f ($_.ProvisionedSpaceGB)}} | Out-File "~\Desktop\without-quickbuild.txt"
