﻿Import-Module  -Name sqlps


#backup MS-CLUSTER

$servers = @(
"GKPT01SQL\CLUSTER",
"GKLAB-156-080\Default"
)

$storages = @(
"\\10.237.156.250\databasebackup\",
"\\gksfiler\nfs\igk\disks\igk_pt_backups\"
)

$retention = 15

$List_ret=@()

###########################################################################


foreach ($server in $servers){
    foreach ($storage in $storages){
        $stor=$storage+$server
        if (!(Test-Path $stor)){ New-Item $stor -Force -Type directory | Out-Null }
        Set-Location "SQLSERVER:\SQL\$server\Databases" 
        ForEach($database in (Get-ChildItem)) {
            $dbName = $database.Name
            $now = Get-Date
            $day = $now.DayOfYear
            $time = $now.TimeOfDay
            $time=$time -Replace(":",".")
            Backup-SqlDatabase -CompressionOption On -Database $dbName -BackupFile "$stor\$dbName.$day.$time.bak"
            #$File_name='{0}.{1}.{2}.bak' -f $dbName,$day,$time
            #$List_bkp += $File_name
            $now_ret =  (Get-Date).AddDays(-$retention)
            $day_ret= $now_ret.DayOfYear
            $File_name_ret='{0}.{1}.bak' -f $dbName,$day_ret
            $List_ret += $File_name_ret
        }
        Set-Location -Path C:\
        foreach ($File_name in $List_ret){Remove-Item  $stor\$File_name* -Force -Verbose }
    }
}



New-Item \\10.237.156.250\databasebackup\mscluster -Force -Type directory | Out-Null
New-Item \\gksfiler\nfs\igk\disks\igk_pt_backups\mscluster -Force -Type directory | Out-Null

Set-Location "SQLSERVER:\SQL\GKPT01SQL\CLUSTER\Databases"

ForEach($database in (Get-ChildItem)) {
$dbName = $database.Name
$now = Get-Date
$day = $now.DayOfYear
$time = $now.TimeOfDay
$time=$time -Replace(":",".")
Backup-SqlDatabase -CompressionOption On -Database $dbName -BackupFile "\\10.237.156.250\databasebackup\mscluster\$dbName.$day.$time.bak"
$File_name='{0}.{1}.{2}.bak' -f $dbName,$day,$time
$List_bkp += $File_name

$now_ret =  (Get-Date).AddDays(-7)
$day_ret= $now_ret.DayOfYear
$File_name_ret='{0}.{1}.bak' -f $dbName,$day_ret
$List_ret += $File_name_ret
}

Set-Location -Path C:\PTScripts
#write-host $List_bkp
foreach ($File_name in $List_bkp){
Copy-Item  \\10.237.156.250\databasebackup\mscluster\$File_name  \\gksfiler\nfs\igk\disks\igk_pt_backups\mscluster -Force -Verbose
}

#Write-Host $List_ret
foreach ($File_name in $List_ret){
Remove-Item  \\10.237.156.250\databasebackup\mscluster\$File_name* -Force -Verbose
}


#backup gklab-156-080

New-Item \\10.237.156.250\databasebackup\gklab-156-080 -Force -Type directory | Out-Null
New-Item \\gksfiler\nfs\igk\disks\igk_pt_backups\gklab-156-080 -Type directory | Out-Null

Set-Location "SQLSERVER:\SQL\GKLAB-156-080\Default\Databases"

ForEach($database in (Get-ChildItem)) {
$dbName = $database.Name
$now = Get-Date
$day = $now.DayOfYear
$time = $now.TimeOfDay
$time=$time -Replace(":",".")
Backup-SqlDatabase -CompressionOption On -Database $dbName -BackupFile "\\10.237.156.250\databasebackup\GKLAB-156-080\$dbName.$day.$time.bak"
$File_name='{0}.{1}.{2}.bak' -f $dbName,$day,$time
$List_bkp += $File_name

$now_ret =  (Get-Date).AddDays(-7)
$day_ret= $now_ret.DayOfYear
$File_name_ret='{0}.{1}.bak' -f $dbName,$day_ret
$List_ret += $File_name_ret
}


Set-Location -Path C:\PTScripts
#write-host $List_bkp
foreach ($File_name in $List_bkp){
Copy-Item  \\10.237.156.250\databasebackup\GKLAB-156-080\$File_name  \\gksfiler\nfs\igk\disks\igk_pt_backups\GKLAB-156-080 -Force -Verbose
}

#Write-Host $List_ret
foreach ($File_name in $List_ret){
Remove-Item  \\10.237.156.250\databasebackup\GKLAB-156-080\$File_name* -Force -Verbose
}