﻿
$HostArray=New-Object System.Collections.ArrayList
$windows=0
$List = @()
$Akt=0
$dir="test"
$error_1=0
$nazwa=""
$hostcount=10
[System.Collections.ArrayList]$Windows_list = $List


#***************************************************************************
Function Get-FileName($initialDirectory)
{
    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null
    $OpenFileDialog = New-Object System.Windows.Forms.OpenFileDialog
    $OpenFileDialog.initialDirectory = $initialDirectory
    $OpenFileDialog.filter = "all (*.*)| *.*"
    $OpenFileDialog.ShowDialog() | Out-Null
    $OpenFileDialog.filename
    $outputBox31.text=$OpenFileDialog.FileName 
    $nazwa=$OpenFileDialog.filename
    $HostArray= Get-Content $nazwa
    $outputBox32.text=$Hostarray
    write-host $HostArray
}

Function Create-Cred
{


}

Function Run-Restart-Action
{
    $outputBox32.AppendText( "`r`n")
    $outputBox32.AppendText( "Run Script... `r`n")
    $nazwa=$OutputBox31.Text
    $HostArray= Get-Content $nazwa
    $Pth=Get-Location
    $log=$Pth,"res.txt" -join "\"
    #$HostArray= Get-Content "c:\temp\restart.txt"
    Get-Date | Out-File $log
    For ($i=0; $i -lt $HostArray.Length; $i++)
            {
                while ($i -gt $logcount + $hostcount + 1){
                    $logs= Get-Content $log
                    $logcount=($logs).count
                    start-sleep -s 3
                }
                $ip=$HostArray[$i] 
                $outputBox32.AppendText( "Check "+ $ip+ "`r`n" )
                Start-Job -filepath $Pth\t1.ps1 -ArgumentList $ip,$log
                $outputBox32.appendtext($i)
                $outputBox32.AppendText( " - "+ $ip)
                #$outputBox32.appendtext($res)                
                $outputBox32.AppendText( "`r`n")
                #Write-host $ip
            }
            $outputBox32.appendtext("---------- DONE -----------")
 
}


#0***************************************************************************
Add-Type -AssemblyName System.Windows.Forms
$form = New-Object Windows.Forms.Form
$form.Text="Restart Hosts Script"
$form.Size = New-Object Drawing.Size @(800,800)
$form.StartPosition = "CenterScreen"

#1***************************************************************************

$objLabel0 = New-Object System.Windows.Forms.Label
$objLabel0.Location = New-Object System.Drawing.Size(30,20) 
$objLabel0.Size = New-Object System.Drawing.Size(200,20) 
$objLabel0.Text = "BitLocker Unlock User and Password"
$form.Controls.Add($objLabel0)

$objLabel1 = New-Object System.Windows.Forms.Label
$objLabel1.Location = New-Object System.Drawing.Size(30,50) 
$objLabel1.Size = New-Object System.Drawing.Size(60,20) 
$objLabel1.Text = "UserName:"
$form.Controls.Add($objLabel1)

$objLabel2 = New-Object System.Windows.Forms.Label
$objLabel2.Location = New-Object System.Drawing.Size(30,90) 
$objLabel2.Size = New-Object System.Drawing.Size(60,20)
$objLabel2.Text = "Password:"
$form.Controls.Add($objLabel2)

$outputBox1 = New-Object System.Windows.Forms.TextBox 
$outputBox1.Location = New-Object System.Drawing.Size(100,46) 
$outputBox1.Size = New-Object System.Drawing.Size(200,20) 
$outputBox1.MultiLine = $False 
$outputBox1.Text = ".\Administrator"
$form.Controls.Add($outputBox1)

$outputBox2 = New-Object System.Windows.Forms.TextBox 
$outputBox2.Location = New-Object System.Drawing.Size(100,86) 
$outputBox2.Size = New-Object System.Drawing.Size(200,20) 
$outputBox2.MultiLine = $False 
$outputBox2.PasswordChar = '*'
$form.Controls.Add($outputBox2)

#2***************************************************************************
$objLabel20 = New-Object System.Windows.Forms.Label
$objLabel20.Location = New-Object System.Drawing.Size(430,20) 
$objLabel20.Size = New-Object System.Drawing.Size(200,20) 
$objLabel20.Text = "QBA LDAP User and Password"
$form.Controls.Add($objLabel20)

$objLabel21 = New-Object System.Windows.Forms.Label
$objLabel21.Location = New-Object System.Drawing.Size(430,50) 
$objLabel21.Size = New-Object System.Drawing.Size(60,20) 
$objLabel21.Text = "UserName:"
$form.Controls.Add($objLabel21)

$objLabel22 = New-Object System.Windows.Forms.Label
$objLabel22.Location = New-Object System.Drawing.Size(430,90) 
$objLabel22.Size = New-Object System.Drawing.Size(60,20)
$objLabel22.Text = "Password:"
$form.Controls.Add($objLabel22)

$outputBox21 = New-Object System.Windows.Forms.TextBox 
$outputBox21.Location = New-Object System.Drawing.Size(500,46) 
$outputBox21.Size = New-Object System.Drawing.Size(200,20) 
$outputBox21.MultiLine = $False
$outputBox21.Text = "ldap" 
$form.Controls.Add($outputBox21)

$outputBox22 = New-Object System.Windows.Forms.TextBox 
$outputBox22.Location = New-Object System.Drawing.Size(500,86) 
$outputBox22.Size = New-Object System.Drawing.Size(200,20) 
$outputBox22.MultiLine = $False 
$outputBox22.PasswordChar = '*'
$form.Controls.Add($outputBox22)

#3******************************************************************************
$Openbtn = New-Object System.Windows.Forms.Button
$Openbtn.add_click({Get-FileName  })
$Openbtn.Text = "Hosts"
$Openbtn.Location = New-Object System.Drawing.Size(30,140)
$form.Controls.Add($Openbtn)

$outputBox31 = New-Object System.Windows.Forms.TextBox 
$outputBox31.Location = New-Object System.Drawing.Size(120,140) 
$outputBox31.Size = New-Object System.Drawing.Size(300,20) 
$outputBox31.MultiLine = $False 
$outputBox31.ReadOnly = $True
$form.Controls.Add($outputBox31)

$outputBox32 = New-Object System.Windows.Forms.TextBox 
$outputBox32.Location = New-Object System.Drawing.Size(30,190) 
$outputBox32.Size = New-Object System.Drawing.Size(725,550) 
$outputBox32.MultiLine = $True 
$outputBox32.ScrollBars = "Vertical" 
$outputBox32.ReadOnly = $True
$form.Controls.Add($outputBox32)

$Runbtn31 = New-Object System.Windows.Forms.Button
$Runbtn31.add_click({Run-Restart-Action })
$Runbtn31.Text = "Run"
$Runbtn31.Location = New-Object System.Drawing.Size(560,140)
$form.Controls.Add($Runbtn31)

#******************************************************************************


$drc = $form.ShowDialog()