﻿
Connect-VIServer gklab-157-005

$linux=0 
$windows=0
$List = @()
$Akt=0
$dir=""
$ListIP = @()
$Pth = split-path -parent $MyInvocation.MyCommand.Definition
Set-Location -Path $Pth

$List=@()
[System.Collections.ArrayList]$Windows_list = $List
[System.Collections.ArrayList]$Linux_list = $List
[System.Collections.ArrayList]$List_summary = $List
#$Windows_list=@()
#$Linux_list=@()
#$list_summary = @() 

#####################################################################################################
$root_name='root'
$sec_root_pwd = '[d0n0tchang3]' | ConvertTo-SecureString -AsPlainText -Force 
$GC = New-Object System.Management.Automation.PSCredential -ArgumentList $root_name, $sec_root_pwd
#####################################################################################################


Function IP_List() {
$ListIP = @()
$ip=''
$i=1
$pref_a='10.237.156.'
$pref_b='10.237.157.'
$pref_c='10.91.78.'
$pref_d='10.91.79.'
$i=1
while ($ip -ne '10.237.156.255') { $ip=$pref_a+$i;$i++;$ListIP += $ip}
$i=0
while ($ip -ne '10.237.157.254') { $ip=$pref_b+$i;$i++;$ListIP += $ip}
$i=1
while ($ip -ne '10.91.78.255') { $ip=$pref_c+$i;$i++;$ListIP += $ip}
$i=0
while ($ip -ne '10.91.79.254') { $ip=$pref_d+$i;$i++;$ListIP += $ip}
return $ListIP 
}

$ListIP = IP_List

#write-host $ListIP[1]
#read-host

Clear-Host
Write-Host ""
#tu mamy adresy IP
  Foreach($ip in $ListIP)
   {
     #$ip=$IPArray[$i] 
     Write-Host -NonewLine 'Check '$ip 
     $test=Test-Connection $ip -count 1 -quiet
     #sprawdzamy ICMP czy jest host
     if (!$test) { Write-Host ' Not responding'}
     if ($test)
        {
        $tcpClient22 = New-Object System.Net.Sockets.TCPClient
        $tcpClient3389 = New-Object System.Net.Sockets.TCPClient
        try { $tcpClient22.Connect($ip,22) | out-null }
        #sprawdzamy czy ma otwarty port 22 - domyslnie unix
        catch { }

        if ($tcpClient22.Connected) {
                $linux++ 
                Write-Host -foregroundcolor green " Linux"
                $res = [System.Net.Dns]::gethostentry($ip)
                $name_host=$res.HostName
                if ($name_host -like '*igk*'){$name_host=$name_host.replace(".igk.intel.com","")}
                if ($name_host -like '*corp*'){$name_host=$name_host.replace("ger.corp.intel.com","")}
                write-host $name_host
                $Linux_list.Add($ip) | out-null
                Get-VM $name_host  | Invoke-VMScript -GuestCredential $GC "/localdisk/processteam/qba1/bin/agent.sh start"
                Get-VM $name_host  | Invoke-VMScript -GuestCredential $GC "/localdisk/processteam/qba2/bin/agent.sh start"
                
                }
        if (!$tcpClient22.Connected) 
            {
                 try { $tcpClient3389.Connect($ip,3389) | out-null }
                 #sprawdzamy czy ma otwarty port 3389 i nieotwarty 22 - zakladamy windows
                 catch { }
                 if ($tcpClient3389.Connected)
                    {
                    
                              write-host " Windows"
                    }
            }
        }
    } 

Write-Host ""
Write-Host -foregroundcolor yellow '--------------------------------------------------' 
Write-Host -foregroundcolor yellow 'Linux:' $linux #-NoNewLine
#Write-Host -foregroundcolor yellow ' Windows:' $Windows
#Write-Host -foregroundcolor yellow ' Windows:' $
#Write-Host -foregroundcolor red 'Zaktualizowano:' $Akt
Write-Host -foregroundcolor yellow '--------------------------------------------------' 
#$List_summary | Out-File c:\temp\Big_Fix_List3.csv
#$list_summary.GetEnumerator() | Export-Csv c:\temp\Big_Fix_List2.csv

