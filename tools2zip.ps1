﻿Set-Item MaxMemoryPerShellMB 300
Set-Item WSMan:\localhost\Shell\MaxMemoryPerShellMB 1000000
Set-Item WSMan:\localhost\Plugin\Microsoft.PowerShell\Quotas\MaxMemoryPerShellMB 1000000
Restart-Service WinRM


$path = '\\gklab-156-017\c$\tools'
#$pathd = '\\storage\salt_tools\windows-tools\zipped-tools'
$pathd = '\\gklab-156-250\pub\tools'
$list = Get-ChildItem $path
foreach ($li in $list){
    write-host $li
    $path_c = $path + '\' + $li
    $path_d = $pathd + '\' + $li
    Write-Host $path_c
    $list_c = Get-ChildItem $path_c
    #write-host $list_c
    foreach ($lic in $list_c) {
        $dest = '{0}\{1}.zip' -f $path_d,$lic
        $src = '{0}\{1}' -f $path_c,$lic
        #Write-Host $src $dest
        if ($lic -notlike 'latest'){ 
        if (!(Test-Path $path_d)) { New-Item -ItemType directory -Path  $path_d } 
        Compress-Archive -Path $src -DestinationPath $dest 
        Write-Host $src $dest $lic
        }
    }
}