﻿$clusterName = "PT-Cluster"
$dc = Get-Datacenter -Cluster $clusterName
foreach($rp in (Import-Csv c:\temp\reslist.txt -UseCulture)){
  $parent = Get-Inventory -Name $rp.Parent -Location $dc
  Try {
    Get-ResourcePool -Name $rp.Name -Location $parent -ErrorAction Stop | Out-Null
  }
  Catch {
    #New-ResourcePool -Name $rp.Name -Location $parent | Out-Null
    #New-Folder -Name ($rp.Name+'_')
    Write-Host $rp.Name
  }
}