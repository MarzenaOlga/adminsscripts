﻿#$path = 'C:\tools'
$comp = 'gklab-79-003'
$sciezka = '\c$\tools'
$sciezka_local = 'c:\tools'
$pathd = '\\{0}{1}' -f $comp,$sciezka
#$path = '\\gklab-156-017\c$\tools'
$path = 'e:\'
#$pathd = '\\storage\salt_tools\windows-tools\zipped-tools'
#$pathd = '\\gklab-156-250\c$\tools'
$list = Get-ChildItem $path
foreach ($li in $list){
    write-host $li
    $path_c = $path + '\' + $li
    $path_d = $pathd + '\' + $li
    $path_l = $sciezka_local +'\' + $li
    Write-Host $path_c
    $list_c = Get-ChildItem $path_c
    #write-host $list_c
    foreach ($lic in $list_c) {
        $dest = '{0}\{1}' -f $path_d,$lic
        $src = '{0}\{1}' -f $path_c,$lic
        #Write-Host $src $dest
        if ($lic -notlike 'latest'){ 
            if (!(Test-Path $path_d)) { New-Item -ItemType directory -Path  $path_d } 
            copy-Item $src $dest -Recurse -Force | Out-Null
            #Write-Host $src 
            #Write-host $dest 
            #Write-host $lic
        }
        if ($lic -like 'latest'){
            Write-host -foregroundcolor yellow $src
            $info = Get-Item $src
            Write-host $info.Target
            $sym_link = $info.Target 
            $Get_link='{0}\{1}' -f $path_l,'latest'
            try{ $script_block_r = [scriptblock]::Create(“& cmd.exe /c  'rmdir $Get_link' ”) }
            catch{}
            Write-Host -foregroundcolor red $script_block_r
            Invoke-Command -Computername $comp -ScriptBlock $script_block_r
            
            $script_block = [scriptblock]::Create(“& cmd.exe /c 'mklink /d $Get_link $sym_link ' ”)
            Write-Host -foregroundcolor green $script_block
            Invoke-Command -Computername $comp  -ScriptBlock  $script_block
            Write-host -foregroundcolor yellow ("Symbolic link created for "+$Get_link+" <<===>> " +$sym_link)
        
        }
    }
}

