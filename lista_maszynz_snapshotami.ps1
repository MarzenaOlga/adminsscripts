﻿#install-module -Name (Get-module -name VMware* -ListAvailable)
Import-Module VMware.VumAutomation

Connect-VIServer gklab-157-005

[System.Collections.ArrayList]$list = (Get-vm | Get-Snapshot).VM.Name

Write-Host $list
 
$list | Out-File c:\temp\vm_with_snap.txt

Disconnect-VIServer -Force