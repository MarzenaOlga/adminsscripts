﻿#Import-Module 'ActiveDirectory'
Import-Module -Name Scripts\Initialize-PowerCLIEnvironment.ps1
Set-PowerCLIConfiguration -WebOperationTimeoutSeconds 1200 -Confirm:$false

$root_name='administrator@vsphere.local'
$sec_root_pwd = 'password' | ConvertTo-SecureString -AsPlainText -Force 
$GC = New-Object System.Management.Automation.PSCredential -ArgumentList $root_name, $sec_root_pwd

Connect-VIServer gklab-157-005 -Credential $GC


$host_list =( 
"gklab-157-005",
"gklab-157-241",
"git-master-new", 
"git-slave-new",
"gklab-156-238",
"gklab-78-122", 
"gklab-156-254",
"percona-master-new")

$retention = 5
$Name_Datastore = "NFS"
$backup = "\\10.237.156.250\d$\NFS"

foreach ($Name_Source_VM in $host_list){
$now = Get-Date
$day = $now.DayOfYear
$time = $now.TimeOfDay
$time=$time -Replace(":",".")
$Name_New_VM=$Name_Source_VM + "_" + $day +"_"+$time
Write-host ($Name_Source_VM+" -> "+$Name_New_VM+" -> "+$Name_Datastore)
New-VM -Name $Name_New_VM -VM $Name_Source_VM -Datastore $Name_Datastore -ResourcePool 'PT-Cluster'

# unregister VM in VCenter
$check = Get-VM $Name_New_VM
if ($check) {Remove-VM $Name_New_VM -Confirm:$false }
########################################################################################################

# retention policy
$retention_time = (Get-Date).AddDays(-$retention)
$retention_day = $retention_time.DayOfYear
#Write-Host $retention_day
$directory_name_retention='{0}_{1}' -f  $Name_Source_VM,$retention_day
#Write-Host $directory_name_retention
$TP=Test-Path $backup\$directory_name_retention*
if ($TP){
    Write-host "Remove " $directory_name_retention
    #$List_retention += $directory_name_retention
    Remove-Item  $backup\$directory_name_retention* -Force -Verbose -Recurse -Confirm:$false
    }
#######################################################################################################
Write-Host '##############################################################################################################################'
}



