﻿Connect-VIServer gklab-157-005

$excludelist = ('QuickBuild - gklab-156-112','VMWare vCenter - gklab-157-005','percona-master','percona-slave1')

[System.Collections.ArrayList]$list = (Get-VM | where { $_.PowerState -eq "PoweredOn" } |sort).Name

foreach ($s in $excludelist ){ $list.Remove($s) }

$list |out-file c:\temp\vm_poweredon_list.txt


