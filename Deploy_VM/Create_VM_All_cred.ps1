﻿Import-Module -Name Scripts\Initialize-PowerCLIEnvironment.ps1
Import-Module 'ActiveDirectory'
Connect-VIServer gklab-157-005

$Pth = split-path -parent $MyInvocation.MyCommand.Definition
Set-Location -Path $Pth


#############################################################################################################
$KeyFile = "c:\passwordstore\AES.key"
$key = Get-Content $KeyFile
#############################################################################################################
$RootUser = "root"
$RootPasswordFile = "c:\passwordstore\Root_Pass.txt"
$GC = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $RootUser, (Get-Content $RootPasswordFile | ConvertTo-SecureString -Key $key)
#############################################################################################################
$AdmUser = "administrator"
$AdmPasswordFile = "c:\passwordstore\Adm_Pass.txt"
$Credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $AdmUser, (Get-Content $AdmPasswordFile | ConvertTo-SecureString -Key $key)
#############################################################################################################
$GKBUser = "gkblditp"
$GKBPasswordFile = "c:\passwordstore\GKB_Pass.txt"
$gkblditp_cred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $GKBUser, (Get-Content $GKBPasswordFile | ConvertTo-SecureString -Key $key)
#############################################################################################################

#***************************************************************************************************************************************************************

Function IP_List() {
[System.Collections.ArrayList]$ListIP = @()
$ip=''

$pref_a='10.237.156.'
$pref_b='10.237.157.'
$pref_c='10.91.78.'
$pref_d='10.91.79.'

$i=6
while ($ip -ne '10.91.78.255') { $ip=$pref_c+$i;$i++;$ListIP += $ip}
$i=0
while ($ip -ne '10.91.79.254') { $ip=$pref_d+$i;$i++;$ListIP += $ip}
$i=0
while ($ip -ne '10.237.157.254') { $ip=$pref_b+$i;$i++;$ListIP += $ip}
$i=6
while ($ip -ne '10.237.156.255') { $ip=$pref_a+$i;$i++;$ListIP += $ip}


$hlist = Get-ResourcePool -Name "Infrastructure" | Get-VM | Sort-Object 
$s = @('10.237.156.104','10.237.156.105','10.237.156.107','10.237.156.108','10.237.156.146','10.237.156.147','10.237.156.148','10.237.156.174','10.237.156.175','10.237.156.176','10.237.156.221','10.237.156.222','10.237.156.225','10.237.156.226','10.237.156.80','10.237.156.100','10.237.156.85','10.237.156.86','10.237.156.87','10.237.156.250','10.237.156.112')
foreach ($a in $hlist){ $s += (Get-VM $a | Select @{N=”IP_Address”;E={@($_.guest.IPAddress[0])}}).IP_Address }
foreach ($zz in $s ){ $ListIP.Remove($zz) }

return $ListIP 
}


Function Host_name([string]$id_ip) {

$test2=$id_ip.Substring(0,5)

if ($test2 -eq '10.91'){
$g3=$id_ip.substring(6,2)
$g4=$id_ip.substring(9)}

if ($test2 -eq '10.23'){
$g3=$id_ip.substring(7,3)
$g4=$id_ip.substring(11)}

$g0='gklab'
if ($g4.Length -eq 1) {$g4 = '00{0}' -f $g4}
if ($g4.Length -eq 2) {$g4 = '0{0}' -f $g4}
$VMname='{0}-{1}-{2}' -f $g0,$g3,$g4

return $VMname
}

Function MacAddress([string]$id_ip)
{
#$id_ip = '10.237.156.3'
#$id_ip = '10.91.78.245'
$id_ip2 = $id_ip.replace('.','')

$test2=$id_ip.Substring(0,5)
#Write-Host $id_ip2.Length
#Write-Host $id_ip2

if ($test2 -eq '10.91'){
    if ($id_ip2.Length -eq 7){
        $m1='0{0}' -f $id_ip2.substring(4,1)
        $m2='{0}0' -f $id_ip2.substring(5,1)
        $m3='0{0}' -f $id_ip2.substring(6,1) }
    if ($id_ip2.Length -eq 8){
        $m1='0{0}' -f $id_ip2.substring(4,1)
        $m2='{0}0' -f $id_ip2.substring(5,1)
        $m3='{0}{1}' -f $id_ip2.substring(6,1), $id_ip2.substring(7,1) }
    if ($id_ip2.Length -eq 9){
        $m1='0{0}' -f $id_ip2.substring(4,1)
        $m2='{0}{1}' -f $id_ip2.substring(5,1), $id_ip2.substring(6,1)
        $m3='{0}{1}' -f $id_ip2.substring(7,1), $id_ip2.substring(8,1) }
}

if ($test2 -eq '10.23'){
    if ($id_ip2.Length -eq 9){
        $m1='1{0}' -f $id_ip2.substring(6,1)
        $m2='{0}0' -f $id_ip2.substring(7,1)
        $m3='0{0}' -f $id_ip2.substring(8,1) }
    if ($id_ip2.Length -eq 10){
        $m1='1{0}' -f $id_ip2.substring(6,1)
        $m2='{0}0' -f $id_ip2.substring(7,1)
        $m3='{0}{1}' -f $id_ip2.substring(8,1), $id_ip2.substring(9,1) }
    if ($id_ip2.Length -eq 11){
        $m1='1{0}' -f $id_ip2.substring(6,1)
        $m2='{0}{1}' -f $id_ip2.substring(7,1), $id_ip2.substring(8,1)
        $m3='{0}{1}' -f $id_ip2.substring(9,1), $id_ip2.substring(10,1) }
}

$m0='00:50:56'
$MacAdd='{0}:{1}:{2}:{3}' -f $m0,$m1,$m2,$m3
#Write-Host $MacAdd
return $MacAdd

}


Function Epsilon {
$ret_epsilon=@()
$ho1 = $null
$a = IP_List
#write-host $a
write-host #########################################################################################################################################################
    foreach ($vm in $a){ 
        $test = Test-Connection $vm -Count 1 -quiet 
        if (!$test) {
            #write-host $vm
            $ho=Host_name($vm)
            $ho1=Get-VM $ho -ErrorAction SilentlyContinue 
            if ($ho1 -eq $null ){
                #write-host 'free'
                $ret_epsilon += $ho
                $ret_epsilon += MacAddress($vm)
                $ret_epsilon += $vm
                break
                }
            #else { Write-Host $ho1.Name}
            }  
    }
#write $ret_epsilon
return $ret_epsilon
}




#***************************************************************************************************************************************************************

Function Select_template {
$tmpl_list = Get-ResourcePool -Name "Templates" | Get-VM | Sort-Object 
$a=$tmpl_list.Length
  
For ($i=0; $i -lt $tmpl_list.Length; $i++){
 Write-Host  $i," - ", $tmpl_list[$i]
 }

$Name_Source_VM = ''
$indx=-1

while ($indx -lt 0){
try{
    $indx = Read-Host -Prompt  'Select template '
    if ($indx -eq '') {$indx='wrong'} 
    [int]$indx = [convert]::ToInt32($indx)   
    if (($indx -lt 0)  -or ($indx -gt $a) ){$indx='minus'} 
    [int]$indx = [convert]::ToInt32($indx)
    }
catch {Write-Host "Select template 0 - $($a-1) "; $indx=-1 }
}
return $tmpl_list[$indx]
Write-Host $Name_Source_VM
}

#***************************************************************************************************************************************************************

#choice Template
$Name_Source_VM=Select_template
write-host $Name_Source_VM
Write-Host "*****************************************"

#Get Ticket ID and Description
$VM_ticket_id = Read-Host -Prompt "Please enter ticket ID (i.e. PTS-12345)"
$VM_description = Read-Host -Prompt "Please enter description of VM for further maintenance "

#Add disk for workspace
$Hard_disk = -1
while ($Hard_disk -lt 0){
try{
    $Hard_disk = Read-Host -Prompt 'Size of Workspace (GB) (0-default-none) '
    [int]$Hard_disk = [convert]::ToInt32($Hard_disk)   
    if ($Hard_disk -lt 0){$Hard_disk='minus'} 
    [int]$Hard_disk = [convert]::ToInt32($Hard_disk)
    
    }
catch {Write-Host "Please enter workspace space bigger or equal to 0 "; $Hard_disk=-1 }
}
write-host 'Size=', $Hard_disk

#Create IP reservation, get ID and macaddress 
$ret_epsilon = Epsilon
$Name_New_VM=$ret_epsilon[0]
$Mac_Address=$ret_epsilon[1]
$IP_New_VM=$ret_epsilon[2]
Write-Host $ret_epsilon
#Start-Sleep 120

#choice distribution switch
if ($Name_New_VM.Substring(6,2) -eq 15) { $Network_Name='ProcessTeam' } else {$Network_Name='ProcessTeam-sustaining-678'}

# choice SAN datastore
$san = Get-Cluster 'PT-Cluster' | Get-Datastore -Name "VNX*" | sort -Property FreeSpaceGB -Descending 
$Name_Datastore = $san[0]

Write-Host "*****************************************"
Write-Host "Summary :"
Write-Host "*****************************************"
Write-Host "VM IP : ", $IP_New_VM
Write-Host "VM name : ", $Name_New_VM
write-host "VM macaddress : ", $Mac_Address 
Write-Host "VM switch : ", $Network_Name
Write-Host "VM Datastore : ", $Name_Datastore
Write-Host "VM Template : ",$Name_Source_VM
Write-Host "VM workspace size : ", $Hard_disk
Write-Host "VM Ticket ID : ", $VM_ticket_id
Write-Host "VM Description : ", $VM_description
Write-Host "*****************************************"

New-VM -Name $Name_New_VM -VM $Name_Source_VM.Name -Datastore $Name_Datastore.Name -ResourcePool 'PT-Cluster'
#New-VM -Name $Name_New_VM -VM $Name_Source_VM.Name -Datastore $Name_Datastore.Name  -vmhost "Gklab-156-102.igk.intel.com" #-ResourcePool Resources #"PT-Cluster\Infrastructure" 

for ($i=1;$i -lt 300;$i++){ start-sleep -s 1
Write-Progress -Activity "Set settings" -PercentComplete  $(($i/300)*100) } 

Get-VM $Name_New_VM | Set-Annotation -CustomAttribute "ticket-id" -Value $VM_ticket_id
Get-VM $Name_New_VM | Set-Annotation -CustomAttribute "description" -Value $VM_description
Get-VM $Name_New_VM |Get-NetworkAdapter| Set-NetworkAdapter -MacAddress $Mac_Address -NetworkName $Network_Name -Confirm:$false

if ($Hard_disk -gt 0) { New-HardDisk -VM $Name_New_VM -CapacityGB $Hard_disk -DiskType Flat -StorageFormat Thin } 
else {write-host "Do not create workspace"}

$ComputerName=$Name_New_VM.Trim() 
$ComputerIP = $IP_New_VM.Trim()

Move-Vm -VM $ComputerName -Destination orphaned
Start-Vm $ComputerName

#Wait for the host to start because host is not running Get-VMGuest does not return values (just as VMware Tools is not installed)
for ($i=1;$i -lt 120;$i++){ start-sleep -s 1
Write-Progress -Activity "Start VM" -PercentComplete  $(($i/120)*100) } 


#############################################################################################
# OS choice


$typeOS = Get-VMGuest -VM $ComputerName  | Select-Object GuestFamily


Write-Host $typeOS.GuestFamily

if ($typeOS.GuestFamily -like "*linux*") {Write-Host -foregroundcolor green " Linux"; $Os = 0 }
if ($typeOS.GuestFamily -like "*windows*") {Write-Host -foregroundcolor green " Windows"; $Os = 1 }



#############################################################################################
# OS choice

 #$tcpClient22 = New-Object System.Net.Sockets.TCPClient
 #$tcpClient3389 = New-Object System.Net.Sockets.TCPClient

 #try { $tcpClient22.Connect($ComputerIP,22) # | out-null 
 #}
 #sprawdzamy czy ma otwarty port 22 - domyslnie unix
 #catch { }
 #if ($tcpClient22.Connected) {Write-Host -foregroundcolor green " Linux"; $Os = 0 }
 
# try {$tcpClient3389.Connect($ComputerIP,3389) #| out-null 
 #}
 #sprawdzamy czy ma otwarty port 3389 - zakladamy windows
 #catch { }
 #if ($tcpClient3389.Connected) {Write-Host -foregroundcolor green " Windows"; $Os = 1 }

##############################################################################################

if ($Os -eq 1 ){ 
    write-host "Windows"
    #remove old computer account
    Get-VM gklab-79-000  | Invoke-VMScript -GuestCredential $GC "/root/rem_acc.sh $ComputerName" -ScriptType Bash
    #try {Remove-ADComputer -Identity $ComputerName -Confirm:$false |out-null }
    #catch {write-host "No domain account"}
    #set rdp connecion
    (Get-WmiObject -class Win32_TSGeneralSetting -Namespace root\cimv2\terminalservices -ComputerName $ComputerIP -Filter "TerminalName='RDP-tcp'").SetUserAuthenticationRequired(0)
    #add new computer account
    New-ADComputer -Name $ComputerName -SAMAccountName $ComputerName -ManagedBy "ITP Process Team Admins" -Description 'owner:ITP Process Team Admins' -DNSHostName $ComputerName.ger.corp.intel.com -Path "OU=Web,OU=Servers,OU=IGK,OU=Customer Labs,OU=Engineering Computing,OU=Resources,DC=ger,DC=corp,DC=intel,DC=com" -Enabled $True -Location "IGK-GDANSK"
    #check ad computer account
    Get-ADComputer $ComputerName 
    #set hostname
    Rename-Computer -ComputerName $ComputerIP -NewName $ComputerName -LocalCredential $Credential
    Restart-Computer $ComputerIP -Force -Credential $Credential -Wait
    #add host to domain
    $usr=[Security.Principal.WindowsIdentity]::getcurrent().name
    for ($i=1;$i -lt 120;$i++){ start-sleep -s 1
    Write-Progress -Activity "Restart VM" -PercentComplete  $(($i/120)*100) } 
    Add-Computer -ComputerName $ComputerIP -DomainName 'ger.corp.intel.com' -LocalCredential $Credential -Force -Credential $usr 
    Restart-Computer -ComputerName $ComputerName -Force -Wait -For WinRM
 
    if ($Hard_disk -gt 0){
        
        Invoke-Command -ComputerName $ComputerName -ScriptBlock {  Get-Disk  | Where partitionstyle -eq 'raw' | Initialize-Disk -PartitionStyle GPT -PassThru | New-Partition -AssignDriveLetter -UseMaximumSize | Format-Volume -FileSystem NTFS -NewFileSystemLabel "UnEncrypted" -Confirm:$false }
        Copy-Item \\gklab-156-250\stuff\qba2015\qba1 \\$ComputerIP\d$\qba1\ -Force -Recurse
        Invoke-Command -ComputerName $ComputerName -ScriptBlock { icacls D:\qba1 /grant '"GER\gkblditp":(OI)(CI)F' }
        Invoke-Command -ComputerName $ComputerName -ScriptBlock {  New-Service -Name "QuickBuild Build Agent 1" -BinaryPathName "D:\qba1\bin\wrapper-windows-x86-64.exe -s D:\qba1\conf\wrapper.conf" -StartupType Automatic -Credential $gkblditp_cred -Description "QuickBuild Build Agent 1" -DisplayName "QuickBuild Build Agent 1" } 
        get-service -ComputerName $ComputerName -Name "QuickBuild Build Agent 1" | Start-Service
    } 
    }

if ($Os -eq 0){
    write-host "Linux"
    #remove old computer account in AD    
    Get-VM gklab-79-000  | Invoke-VMScript -GuestCredential $GC "/root/rem_acc.sh $ComputerName" -ScriptType Bash
    #disable enforce, change ssh keys, set hostname
    Get-VM $ComputerName  | Invoke-VMScript -GuestCredential $GC "setenforce 0" -ScriptType Bash
    Get-VM $ComputerName  | Invoke-VMScript -GuestCredential $GC "rm -rfv /etc/ssh/ssh_host* " -ScriptType Bash
    Get-VM $ComputerName  | Invoke-VMScript -GuestCredential $GC "ssh-keygen -q -A -f /etc/ssh/ " -ScriptType Bash
    Get-VM $ComputerName  | Invoke-VMScript -GuestCredential $GC "hostnamectl set-hostname '$ComputerName'" -ScriptType Bash
    Get-VM $ComputerName  | Invoke-VMScript -GuestCredential $GC "service vasd stop" -ScriptType Bash
    Get-VM $ComputerName  | Invoke-VMScript -GuestCredential $GC "wget -q --no-check-certificate -O /tmp/krb5_setup.sh http://10.237.156.250/scripts/krb5_setup.sh" -ScriptType Bash
    Get-VM $ComputerName  | Invoke-VMScript -GuestCredential $GC "chmod 0700 /tmp/krb5_setup.sh && /tmp/krb5_setup.sh -s igk" -ScriptType Bash
    Get-VM $ComputerName  | Invoke-VMScript -GuestCredential $GC "keytabs.sh install" -ScriptType Bash
    Get-VM $ComputerName  | Invoke-VMScript -GuestCredential $GC "keytabs.sh" -ScriptType Bash
    Get-VM $ComputerName  | Invoke-VMScript -GuestCredential $GC "shutdown -r 1 & " -ScriptType Bash

    }



