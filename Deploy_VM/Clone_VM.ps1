﻿Import-Module -Name Scripts\Initialize-PowerCLIEnvironment.ps1
Import-Module 'ActiveDirectory'
Connect-VIServer gklab-157-005

$Pth = split-path -parent $MyInvocation.MyCommand.Definition
Set-Location -Path $Pth


#############################################################################################################
$KeyFile = "c:\passwordstore\AES.key"
$key = Get-Content $KeyFile
#############################################################################################################
$RootUser = "root"
$RootPasswordFile = "c:\passwordstore\Root_Pass.txt"
$GC = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $RootUser, (Get-Content $RootPasswordFile | ConvertTo-SecureString -Key $key)
#############################################################################################################
$AdmUser = "administrator"


$AdmPasswordFile = "c:\passwordstore\Adm_Pass.txt"
$Credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $AdmUser, (Get-Content $AdmPasswordFile | ConvertTo-SecureString -Key $key)
#############################################################################################################
$GKBUser = "gkblditp"
$GKBPasswordFile = "c:\passwordstore\GKB_Pass.txt"
$gkblditp_cred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $GKBUser, (Get-Content $GKBPasswordFile | ConvertTo-SecureString -Key $key)
#############################################################################################################

#***************************************************************************************************************************************************************

Function IP_List() {
[System.Collections.ArrayList]$ListIP = @()
$ip=''
$x = 1
 
while ($x -ne 0) {
# -or $x -ne 2 -or $x -ne 3) {
Write-host ("Networks: ")
Write-Host ("1. 156 (10.237.156.0/23) - native - for infrastructure")
Write-Host ("2. 678 (10.91.78.0/23)   - Lab-Sustaining")
Write-Host ("3. 444 (10.91.224.0/23)  - Lab-PT")
$z = Read-Host("Select Network: ")
if (($z -eq 1) -or ($z -eq 2) -or ($z -eq 3)) { $x=0 }
} 


if ($z -eq 1) { 
$pref_a='10.237.156.' 
$pref_b='10.237.157.' }
if ($z -eq 2) {
$pref_a= '10.91.78.'
$pref_b='10.91.79.' }
if ($z -eq 3) {
$pref_a='10.91.224.'
$pref_b='10.91.225.' }

$i=6
while ($i -ne 255) { $ip=$pref_a+$i;$i++;$ListIP += $ip}
$i=0
while ($i -ne 254) { $ip=$pref_b+$i;$i++;$ListIP += $ip}

#foreach ($vm in $ListIP){write-host $vm}

#$i=6
#while ($ip -ne '10.237.156.255') { $ip=$pref_a+$i;$i++;$ListIP += $ip}
#$i=0
#while ($ip -ne '10.237.157.254') { $ip=$pref_b+$i;$i++;$ListIP += $ip}
#$i=6
#while ($ip -ne '10.91.78.255') { $ip=$pref_c+$i;$i++;$ListIP += $ip}
#$i=0
#while ($ip -ne '10.91.79.254') { $ip=$pref_d+$i;$i++;$ListIP += $ip}


$hlist = Get-ResourcePool -Name "Infrastructure" | Get-VM | Sort-Object 
$s = @('10.237.156.104','10.237.156.105','10.237.156.107','10.237.156.108','10.237.156.146','10.237.156.147','10.237.156.148','10.237.156.174','10.237.156.175','10.237.156.176','10.237.156.221','10.237.156.222','10.237.156.225','10.237.156.226','10.237.156.80','10.237.156.100','10.237.156.85','10.237.156.86','10.237.156.87','10.237.156.250','10.237.156.112')
foreach ($a in $hlist){ $s += (Get-VM $a | Select @{N=”IP_Address”;E={@($_.guest.IPAddress[0])}}).IP_Address }
foreach ($zz in $s ){ $ListIP.Remove($zz) }

return $ListIP 
}


Function Host_name([string]$id_ip) {

$test2=$id_ip.Substring(0,7)


if ($test2 -eq '10.91.2'){
$g3=$id_ip.substring(6,3)
$g4=$id_ip.substring(10)}


if ($test2 -eq '10.91.7'){
$g3=$id_ip.substring(6,2)
$g4=$id_ip.substring(9)}

if ($test2 -eq '10.237.'){
$g3=$id_ip.substring(7,3)
$g4=$id_ip.substring(11)}

$g0='gklab'
if ($g4.Length -eq 1) {$g4 = '00{0}' -f $g4}
if ($g4.Length -eq 2) {$g4 = '0{0}' -f $g4}
$VMname='{0}-{1}-{2}' -f $g0,$g3,$g4

return $VMname
}

Function MacAddress([string]$id_ip)
{

$test2=$id_ip.Substring(0,7)
$id_ip2 = $id_ip.replace('.','')

if ($test2 -eq '10.91.2'){
    if ($id_ip2.Length -eq 8){
        $m1='2{0}' -f $id_ip2.substring(5,1)
        $m2='{0}0' -f $id_ip2.substring(6,1)
        $m3='0{0}' -f $id_ip2.substring(7,1) }
    if ($id_ip2.Length -eq 9){
        $m1='2{0}' -f $id_ip2.substring(5,1)
        $m2='{0}0' -f $id_ip2.substring(6,1)
        $m3='{0}{1}' -f $id_ip2.substring(7,1), $id_ip2.substring(8,1) }
    if ($id_ip2.Length -eq 10){
        $m1='2{0}' -f $id_ip2.substring(5,1)
        $m2='{0}{1}' -f $id_ip2.substring(6,1), $id_ip2.substring(7,1)
        $m3='{0}{1}' -f $id_ip2.substring(8,1), $id_ip2.substring(9,1) }
}

if ($test2 -eq '10.91.7'){
    if ($id_ip2.Length -eq 7){
        $m1='0{0}' -f $id_ip2.substring(4,1)
        $m2='{0}0' -f $id_ip2.substring(5,1)
        $m3='0{0}' -f $id_ip2.substring(6,1) }
    if ($id_ip2.Length -eq 8){
        $m1='0{0}' -f $id_ip2.substring(4,1)
        $m2='{0}0' -f $id_ip2.substring(5,1)
        $m3='{0}{1}' -f $id_ip2.substring(6,1), $id_ip2.substring(7,1) }
    if ($id_ip2.Length -eq 9){
        $m1='0{0}' -f $id_ip2.substring(4,1)
        $m2='{0}{1}' -f $id_ip2.substring(5,1), $id_ip2.substring(6,1)
        $m3='{0}{1}' -f $id_ip2.substring(7,1), $id_ip2.substring(8,1) }
}

if ($test2 -eq '10.237.'){
    if ($id_ip2.Length -eq 9){
        $m1='1{0}' -f $id_ip2.substring(6,1)
        $m2='{0}0' -f $id_ip2.substring(7,1)
        $m3='0{0}' -f $id_ip2.substring(8,1) }
    if ($id_ip2.Length -eq 10){
        $m1='1{0}' -f $id_ip2.substring(6,1)
        $m2='{0}0' -f $id_ip2.substring(7,1)
        $m3='{0}{1}' -f $id_ip2.substring(8,1), $id_ip2.substring(9,1) }
    if ($id_ip2.Length -eq 11){
        $m1='1{0}' -f $id_ip2.substring(6,1)
        $m2='{0}{1}' -f $id_ip2.substring(7,1), $id_ip2.substring(8,1)
        $m3='{0}{1}' -f $id_ip2.substring(9,1), $id_ip2.substring(10,1) }
}

$m0='00:50:56'
$MacAdd='{0}:{1}:{2}:{3}' -f $m0,$m1,$m2,$m3
#Write-Host $MacAdd
return $MacAdd

}


Function Epsilon {
$ret_epsilon=@()
$ho1 = $null
$a = IP_List
#write-host $a
write-host #########################################################################################################################################################
    foreach ($vm in $a){ 
        $test = Test-Connection $vm -Count 1 -quiet 
        if (!$test) {
            #write-host $vm
            $ho=Host_name($vm)
            $ho1=Get-VM $ho -ErrorAction SilentlyContinue 
            if ($ho1 -eq $null ){
                #write-host 'free'
                $ret_epsilon += $ho
                $ret_epsilon += MacAddress($vm)
                $ret_epsilon += $vm
                break
                }
            #else { Write-Host $ho1.Name}
            }  
    }
#write $ret_epsilon
return $ret_epsilon
}




#***************************************************************************************************************************************************************

Function Select_template_x {
$tmpl_list = Get-ResourcePool -Name "Templates" | Get-VM | Sort-Object 
$a=$tmpl_list.Length
  
For ($i=0; $i -lt $tmpl_list.Length; $i++){
 Write-Host  $i," - ", $tmpl_list[$i]
 }

$Name_Source_VM = ''
$indx=-1

while ($indx -lt 0){
try{
    $indx = Read-Host -Prompt  'Select template '
    if ($indx -eq '') {$indx='wrong'} 
    [int]$indx = [convert]::ToInt32($indx)   
    if (($indx -lt 0)  -or ($indx -gt $a) ){$indx='minus'} 
    [int]$indx = [convert]::ToInt32($indx)
    }
catch {Write-Host "Select template 0 - $($a-1) "; $indx=-1 }
}
return $tmpl_list[$indx]
Write-Host $Name_Source_VM
}


Function Select_template {
    $wynik = $false
    while ($wynik -eq $false){ 
        $VM_name = Read-Host -Prompt  'Select VM '
        $VM_name = $VM_name.Trim()
        $wynik = Get-VM -Name $VM_name
        }
    $VM_Name_s =@{"Name"=$VM_name}
    return $VM_name_s
}

#***************************************************************************************************************************************************************

#choice Template
$Name_Source_VM=Select_template
write-host $Name_Source_VM
Write-Host "*****************************************"

#Get Ticket ID and Description
$VM_ticket_id = Read-Host -Prompt "Please enter ticket ID (i.e. PTS-12345)"
$VM_description = Read-Host -Prompt "Please enter description of VM for further maintenance "

#Add disk for workspace
#$Hard_disk = -1
#while ($Hard_disk -lt 0){
#try{
#    $Hard_disk = Read-Host -Prompt 'Size of Workspace (GB) (0-default-none) '
#    [int]$Hard_disk = [convert]::ToInt32($Hard_disk)   
#    if ($Hard_disk -lt 0){$Hard_disk='minus'} 
#    [int]$Hard_disk = [convert]::ToInt32($Hard_disk)
#    
#    }
#catch {Write-Host "Please enter workspace space bigger or equal to 0 "; $Hard_disk=-1 }
#}

$Hard_disk=0
write-host 'Size=', $Hard_disk

#Create IP reservation, get ID and macaddress 
$ret_epsilon = Epsilon
if ($ret_epsilon -eq $null) {Write-host "No free IP", [Environment]::Exit(1) }
$Name_New_VM=$ret_epsilon[0]
$Mac_Address=$ret_epsilon[1]
$IP_New_VM=$ret_epsilon[2]
Write-Host $ret_epsilon
#Start-Sleep 120

#choice distribution switch
$test3=$IP_New_VM.Substring(0,7)
#write-host $test3
start-sleep 30
if ($test3 -eq '10.91.2'){$Network_Name='ProcessTeamLab-444'}
if ($test3 -eq '10.91.7'){$Network_Name='ProcessTeam-sustaining-678'}
if ($test3 -eq '10.237.'){$Network_Name='ProcessTeam'}

#if ($Name_New_VM.Substring(6,2) -eq 15) { $Network_Name='ProcessTeam' } else {$Network_Name='ProcessTeam-sustaining-678'}

# choice SAN datastore
$san = Get-Cluster 'PT-Cluster' | Get-Datastore -Name "VNX*" | sort -Property FreeSpaceGB -Descending 
$Name_Datastore = $san[0]

Write-Host "*****************************************"
Write-Host "Summary :"
Write-Host "*****************************************"
Write-Host "VM IP : ", $IP_New_VM
Write-Host "VM name : ", $Name_New_VM
write-host "VM macaddress : ", $Mac_Address 
Write-Host "VM switch : ", $Network_Name
Write-Host "VM Datastore : ", $Name_Datastore
Write-Host "VM Template : ",$Name_Source_VM.Name
Write-Host "VM workspace size : ", $Hard_disk
Write-Host "VM Ticket ID : ", $VM_ticket_id
Write-Host "VM Description : ", $VM_description
Write-Host "*****************************************"

New-VM -Name $Name_New_VM -VM $Name_Source_VM.Name -Datastore $Name_Datastore.Name -ResourcePool 'PT-Cluster'
#New-VM -Name $Name_New_VM -VM $Name_Source_VM.Name -Datastore $Name_Datastore.Name  -vmhost "Gklab-156-102.igk.intel.com" #-ResourcePool Resources #"PT-Cluster\Infrastructure" 

for ($i=1;$i -lt 300;$i++){ start-sleep -s 1
Write-Progress -Activity "Set settings" -PercentComplete  $(($i/300)*100) } 

Get-VM $Name_New_VM | Set-Annotation -CustomAttribute "ticket-id" -Value $VM_ticket_id
Get-VM $Name_New_VM | Set-Annotation -CustomAttribute "description" -Value $VM_description
Get-VM $Name_New_VM |Get-NetworkAdapter| Set-NetworkAdapter -MacAddress $Mac_Address -NetworkName $Network_Name -Confirm:$false

if ($Hard_disk -gt 0) { New-HardDisk -VM $Name_New_VM -CapacityGB $Hard_disk -DiskType Flat -StorageFormat Thin } 
else {write-host "Do not create workspace"}

$ComputerName=$Name_New_VM.Trim() 
$ComputerIP = $IP_New_VM.Trim()

Move-Vm -VM $ComputerName -Destination orphaned
Start-Vm $ComputerName

#Wait for the host to start because host is not running Get-VMGuest does not return values (just as VMware Tools is not installed)
for ($i=1;$i -lt 120;$i++){ start-sleep -s 1
Write-Progress -Activity "Start VM" -PercentComplete  $(($i/120)*100) } 


#############################################################################################
# OS choice


$typeOS = Get-VMGuest -VM $ComputerName  | Select-Object GuestFamily


Write-Host $typeOS.GuestFamily

if ($typeOS.GuestFamily -like "*linux*") {Write-Host -foregroundcolor green " Linux"; $Os = 0 }
if ($typeOS.GuestFamily -like "*windows*") {Write-Host -foregroundcolor green " Windows"; $Os = 1 }



#############################################################################################
# OS choice

 #$tcpClient22 = New-Object System.Net.Sockets.TCPClient
 #$tcpClient3389 = New-Object System.Net.Sockets.TCPClient

 #try { $tcpClient22.Connect($ComputerIP,22) # | out-null 
 #}
 #sprawdzamy czy ma otwarty port 22 - domyslnie unix
 #catch { }
 #if ($tcpClient22.Connected) {Write-Host -foregroundcolor green " Linux"; $Os = 0 }
 
# try {$tcpClient3389.Connect($ComputerIP,3389) #| out-null 
 #}
 #sprawdzamy czy ma otwarty port 3389 - zakladamy windows
 #catch { }
 #if ($tcpClient3389.Connected) {Write-Host -foregroundcolor green " Windows"; $Os = 1 }

##############################################################################################

if ($Os -eq 1 ){ 
    write-host "Windows"
    Read-Host -Prompt "IF THIS MACHINE USES DIFFERENCIAL DISK PLEASE DETACH IT NOW"
    #remove old computer account in AD    
    Get-VM gklab-79-000  | Invoke-VMScript -GuestCredential $GC "/root/rem_acc.sh $ComputerName"
    #add new computer account
    New-ADComputer -Name $ComputerName -SAMAccountName $ComputerName -ManagedBy "ITP Process Team Admins" -Description 'owner:rkupniex' -DNSHostName $ComputerName.ger.corp.intel.com -Path "OU=Web,OU=Servers,OU=IGK,OU=Customer Labs,OU=Engineering Computing,OU=Resources,DC=ger,DC=corp,DC=intel,DC=com" -Enabled $True -Location "IGK-GDANSK"
    #check ad computer account
    Get-ADComputer $ComputerName
    
    #disconnect network to left domain
    Get-VM $ComputerName | Get-NetworkAdapter| Set-NetworkAdapter -Connected:$false -StartConnected:$false -Confirm:$false
    
    Restart-VMGuest $ComputerName 
    Start-Sleep -s 20 
    Wait-Tools -VM $ComputerName

    Read-Host -Prompt "Check if comptuter is disconnected from network"
    Get-VM $ComputerName | Invoke-VMScript -GuestCredential $Credential "Remove-Computer -Force" -ScriptType Powershell
    # 
    Restart-VMGuest $ComputerName 
    Start-Sleep -s 20 
    Wait-Tools -VM $ComputerName
    Read-Host -Prompt "Check if comptuter has left domain"

    #change hostname
    Get-VM $ComputerName | Invoke-VMScript -GuestCredential $Credential "Rename-Computer -NewName $ComputerName" -ScriptType Powershell
    
    Restart-VMGuest $ComputerName 
    Start-Sleep -s 20 
    Wait-Tools -VM $ComputerName
    Read-Host -Prompt "Check if computer has correct hostname"
    
    #join domain
    Get-VM $ComputerName | Get-NetworkAdapter| Set-NetworkAdapter -Connected:$true -StartConnected:$true -Confirm:$false
    Add-Computer -ComputerName $ComputerIP -DomainName 'ger.corp.intel.com' -LocalCredential $Credential -Force -Credential $usr -Restart
    } 

if ($Os -eq 0 ){
    write-host "Linux"
    Start-Sleep -s 5
    #remove old computer account in AD    
    Get-VM gklab-79-000  | Invoke-VMScript -GuestCredential $GC "/root/rem_acc.sh $ComputerName" -ScriptType Bash
    Get-VM gklab-79-000  | Invoke-VMScript -GuestCredential $GC "/root/rem_acc.sh $ComputerName.igk.intel.com" -ScriptType Bash
    Get-VM gklab-79-000  | Invoke-VMScript -GuestCredential $GC "/root/rem_acc.sh $ComputerName.ger.corp.intel.com" -ScriptType Bash

    #disable enforce, change ssh keys, set hostname
    Get-VM $ComputerName  | Invoke-VMScript -GuestCredential $GC "setenforce 0" -ScriptType Bash
    Get-VM $ComputerName  | Invoke-VMScript -GuestCredential $GC "rm -rfv /etc/ssh/ssh_host* " -ScriptType Bash
    Get-VM $ComputerName  | Invoke-VMScript -GuestCredential $GC "ssh-keygen -q -A -f /etc/ssh/ " -ScriptType Bash
    Get-VM $ComputerName  | Invoke-VMScript -GuestCredential $GC "hostnamectl set-hostname '$ComputerName'" -ScriptType Bash
    Get-VM $ComputerName  | Invoke-VMScript -GuestCredential $GC "rm -f /etc/salt/minion_id /etc/salt/pki/minion/minion.pem /etc/salt/pki/minion/minion.pub /etc/salt/pki/minion/minion_master.pub" -ScriptType Bash
    Get-VM $ComputerName  | Invoke-VMScript -GuestCredential $GC "service vasd stop" -ScriptType Bash
    Get-VM $ComputerName  | Invoke-VMScript -GuestCredential $GC "wget -q --no-check-certificate -O /tmp/krb5_setup.sh http://10.237.156.250/scripts/krb5_setup.sh" -ScriptType Bash
    Get-VM $ComputerName  | Invoke-VMScript -GuestCredential $GC "chmod 0700 /tmp/krb5_setup.sh && /tmp/krb5_setup.sh -s igk" -ScriptType Bash
    Get-VM $ComputerName  | Invoke-VMScript -GuestCredential $GC "shutdown -r 5 & " -ScriptType Bash

    }


