﻿$KeyFile = "c:\passwordstore\AES.key"
$Key = New-Object Byte[] 16   # You can use 16, 24, or 32 for AES
[Security.Cryptography.RNGCryptoServiceProvider]::Create().GetBytes($Key)
$Key | out-file $KeyFile

$PasswordFile = "c:\passwordstore\Root_Pass.txt"
$KeyFile = "c:\passwordstore\AES.key"
$Key = Get-Content $KeyFile
$Password = Read-Host -Prompt "Enter root password" -AsSecureString
$Password | ConvertFrom-SecureString -key $Key | Out-File $PasswordFile

$PasswordFile = "c:\passwordstore\Adm_Pass.txt"
$KeyFile = "c:\passwordstore\AES.key"
$Key = Get-Content $KeyFile
$Password = Read-Host -Prompt "Enter Admin password" -AsSecureString
$Password | ConvertFrom-SecureString -key $Key | Out-File $PasswordFile

$PasswordFile = "c:\passwordstore\GKB_Pass.txt"
$KeyFile = "c:\passwordstore\AES.key"
$Key = Get-Content $KeyFile
$Password = Read-Host -Prompt "Enter gkblditp password" -AsSecureString
$Password | ConvertFrom-SecureString -key $Key | Out-File $PasswordFile