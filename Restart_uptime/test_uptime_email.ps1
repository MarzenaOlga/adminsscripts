﻿Import-Module 'ActiveDirectory'
Import-Module Posh-SSH
Connect-VIServer gklab-157-005


#############################################################################################################
$KeyFile = "c:\passwordstore\AES.key"
$key = Get-Content $KeyFile
#############################################################################################################
$RootUser = "root"
$RootPasswordFile = "c:\passwordstore\Root_Pass.txt"
$GC = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $RootUser, (Get-Content $RootPasswordFile | ConvertTo-SecureString -Key $key)
#############################################################################################################
$AdmUser = "administrator"


$AdmPasswordFile = "c:\passwordstore\Adm_Pass.txt"
$Credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $AdmUser, (Get-Content $AdmPasswordFile | ConvertTo-SecureString -Key $key)
#############################################################################################################
$GKBUser = "gkblditp"
$GKBPasswordFile = "c:\passwordstore\GKB_Pass.txt"
$gkblditp_cred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $GKBUser, (Get-Content $GKBPasswordFile | ConvertTo-SecureString -Key $key)
#############################################################################################################

$IPArray=New-Object System.Collections.ArrayList
#$IPArray= Get-Content C:\Tools\hosts.txt
$linux=0 
$windows=0
$List = @()
$Akt=0
$Pth = split-path -parent $MyInvocation.MyCommand.Definition
#$Pth="C:\PTScripts"
$log=$Pth,"hosts.txt" -join "\"
$log4=$Pth,"restart_log.txt" -join "\"
$IPArray= Get-Content $log
$HoList = @("host","Uptime")
[System.Collections.ArrayList]$Windows_list = $List
[System.Collections.ArrayList]$Linux_list = $List
[System.Collections.ArrayList]$Res_list = $List


$ListIP = @()

Function IP_List() {
$ListIP = @()
$ip=''
$i=1
$pref_a='10.237.156.'
$pref_b='10.237.157.'
$pref_c='10.91.78.'
$pref_d='10.91.79.'
$i=6
while ($ip -ne '10.237.156.255') { $ip=$pref_a+$i;$i++;$ListIP += $ip}
$i=0
while ($ip -ne '10.237.157.254') { $ip=$pref_b+$i;$i++;$ListIP += $ip}
$i=6
while ($ip -ne '10.91.78.255') { $ip=$pref_c+$i;$i++;$ListIP += $ip}
$i=0
while ($ip -ne '10.91.79.254') { $ip=$pref_d+$i;$i++;$ListIP += $ip}
return $ListIP 
}

$ListExclude=@('10.237.156.80','10.237.156.86','10.237.156.87','10.237.156.100','10.237.156.251','10.237.156.250','10.237.156.101','10.237.156.85','10.237.157.5')
$ListIP = IP_List
[System.Collections.ArrayList]$ListIP2=$ListIP

foreach ($ex in $ListExclude){
#write-host $ex
$ListIP2.Remove($ex)
}

$IPArray=$ListIP2
write-host $IPArray


Clear-Host
Write-Host ""
Write-Host $log
Get-Date | Out-File $log4
#tu mamy adresy IP
Write-Output "Host `t `t `t `t `t `t Uptime" > C:\Temp\uptime.txt
$result=@{}


foreach ($ip in $ListIP2)
    {
     write-Host " "
     Write-Host -NonewLine 'Check '$ip
     $test=Test-Connection $ip -count 1 -quiet
     #read-host

     #sprawdzamy ICMP czy jest host
     
     if (!$test) { Write-Host -NonewLine ' Not responding'}
     if ($test)
        {
        $tcpClient22 = New-Object System.Net.Sockets.TCPClient
        $tcpClient3389 = New-Object System.Net.Sockets.TCPClient
        
        try { $tcpClient22.Connect($ip,22)>$null }
        #sprawdzamy czy ma otwarty port 22 - domyslnie unix
        catch { }

        if ($tcpClient22.Connected) {
            $linux++; Write-Host -NonewLine -foregroundcolor green " Linux   ";$Linux_list.Add($ip)|out-null
            $res = [System.Net.Dns]::gethostentry($ip)
            $name_host=$res.HostName
            if ($name_host -like '*igk*'){$name_host=$name_host.replace(".igk.intel.com"," ")}
            if ($name_host -like '*corp*'){$name_host=$name_host.replace(".ger.corp.intel.com"," ")}
            $ip=$name_host.Trim()
            
            New-SSHSession -ComputerName $ip -Credential $GC -AcceptKey:$true |Out-Null
            $time = Invoke-SSHCommand -SessionId 0 -Command 'uptime'
            $str = $time.Output.substring(13,3)
            $str = $str.replace('d','')
            $str=$str.Trim()
            $str=$str -as [int]
            Write-Host ' Uptime: ', $str -foregroundcolor Cyan -NoNewline
            if ($str -gt 6) { $result.Add($ip,$str) }
            Remove-SSHSession -SessionId 0 |out-null

            
            }
        if (!$tcpClient22.Connected) 
            {
                 try { $tcpClient3389.Connect($ip,3389)>$null }
                 #sprawdzamy czy ma otwarty port 3389 i nieotwarty 22 - zakladamy windows
                 catch { }
                 if ($tcpClient3389.Connected)
                    {
                    $res = [System.Net.Dns]::gethostentry($ip)
                    $name_host=$res.HostName
                    if ($name_host -like '*ger*'){$name_host=$name_host.replace("igk.intel.com","ger.corp.intel.com")}
                
                    $ip=$name_host.Trim()
    
                    $windows++; Write-Host -NonewLine -foregroundcolor yellow  " Windows ";$Windows_list.Add($ip) |out-null;
                    $up=Invoke-Command -Computername $ip  -ScriptBlock{ (get-date) – (gcim Win32_OperatingSystem).LastBootUpTime}
                    if ($up.TotalDays -lt 2) { $ip >> c:\temp\rest.txt }
                    if ($up.TotalDays -gt 6) {
                        Write-Host -NonewLine -foregroundcolor red  " Uptime =" $up.TotalDays " ";  
                        $Res_list.Add($ip) | Out-Null 
                        #write-host $ip,'\t',$up.TotalDays >> 'c:\temp\uptime.txt'
                        Add-Content -Path  C:\Temp\uptime.txt -NoNewline  -Value $ip,`t ,`t,$up.TotalDays
                        Add-Content -Path  C:\Temp\uptime.txt -Value ''
                        #$ScriptBlock = [scriptblock]::Create(“ $Pth\restart_slave_script.ps1 $ip $log4 ”)
                        #Start-Job -ScriptBlock $ScriptBlock -Verbose
                        $HoList += @($ip, $up.TotalDays)
                         }
                    else {Write-Host -NonewLine -foregroundcolor blue  " Uptime =" $up.TotalDays}
                    }
            }
        }
} 
    
Write-Host ""
Write-Host $HoList
Write-Host -foregroundcolor yellow '--------------------------------------------------' 
Write-Host -foregroundcolor yellow 'Linux:' $linux -NoNewLine
Write-Host -foregroundcolor yellow ' Windows:' $Windows
#Write-Host -foregroundcolor yellow ' Uptime:' $Res_list
Write-Host -foregroundcolor yellow '--------------------------------------------------' 
$result.GetEnumerator()|sort-object Name |Export-Csv c:\temp\linux_uptime -UseCulture -NoTypeInformation

$log1=$Pth,"Windows_DNS_list.txt" -join "\"
$log2=$Pth,"Linux_DNS_list.txt" -join "\"
$log3=$Pth,"restart_host_list.txt" -join "\"
$log5=$Pth,"uptime_host_list.txt" -join "\"

$HostsList | Out-File $log5
$Windows_list | Out-File $log1
$Res_list | Out-File $log3
$Linux_list | Out-File $log2
