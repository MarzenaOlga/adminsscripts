﻿$Pth = split-path -parent $MyInvocation.MyCommand.Definition
$list = (
'gklab-79-014',
'gklab-156-067',
'gklab-78-093',
'gklab-157-206',
'gklab-157-123',
'gklab-156-123',
'gklab-157-134',
'gklab-157-036',
'gklab-156-088',
'gklab-79-112',
'gklab-157-121',
'gklab-78-154',
'gklab-157-057',
'gklab-157-235',
'gklab-78-217',
'gklab-78-218',
'gklab-78-220',
'gklab-78-226',
'gklab-78-227',
'gklab-78-228',
'gklab-78-229',
'gklab-79-226',
'gklab-79-025',
'gklab-157-215',
'gklab-156-121',
'gklab-156-122',
'gklab-156-124',
'gklab-156-127',
'gklab-157-132',
'gklab-157-133',
'gklab-157-204',
'gklab-157-208',
'gklab-157-209',
'gklab-157-214'
)

foreach ($ip in $list){
$ScriptBlock = [scriptblock]::Create(“ $Pth\restart_slave_script.ps1 $ip ”)
Start-Job -ScriptBlock $ScriptBlock -Verbose
}

start-sleep -Seconds 180

foreach ($ip in $list){
$up=Invoke-Command -Computername $ip  -ScriptBlock{ (get-date) – (gcim Win32_OperatingSystem).LastBootUpTime}
Write-Output ('{0} {1}' -f $ip, $up.TotalDays)
}
