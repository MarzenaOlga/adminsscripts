﻿$store = $HOME + "\passwordstore\"
if (!(Test-Path $store)){ New-Item -ItemType Directory -Path $HOME -Name "passwordstore" }

$KeyFile = "AES.key"
if (!(Test-Path $store$KeyFile )){
$Key = New-Object Byte[] 16   # You can use 16, 24, or 32 for AES
[Security.Cryptography.RNGCryptoServiceProvider]::Create().GetBytes($Key)
$Key | out-file $store$KeyFile -Force
}

$pass = Read-Host -Prompt "User: "
$PasswordFile = $pass+".pas"
$KeyFile = "AES.key"
$Key = Get-Content $store$KeyFile
$Password = Read-Host -Prompt "Enter root password" -AsSecureString
$Password2  = Read-Host -Prompt "Enter root password" -AsSecureString
if ($Password -eq $Password2) {$Password | ConvertFrom-SecureString -key $Key | Out-File  $store$PasswordFile -Force}
else {Write-Host "Password not match"}