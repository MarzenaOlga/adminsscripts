﻿#Import-Module -Name Scripts\Initialize-PowerCLIEnvironment.ps1
#Import-Module 'ActiveDirectory'
#Connect-VIServer gklab-157-005


#$typeOS = Get-VM gklab-157-150 | Sort | Get-View -Property @("Name", "Config.GuestFullName", "Guest.GuestFullName") | Select -Property Name, @{N="Configured_OS";E={$_.Config.GuestFullName}},  @{N="Running_OS";E={$_.Guest.GuestFullName}} | Format-Table -AutoSize

$typeOS = Get-VMGuest -VM gklab-157-150  | Select-Object OSFullName

Write-Host $typeOS.OSFullName

if ($typeOS.OSFullName -like "*Linux*") {Write-Host "Linux"}
if ($typeOS.OSFullName -like "*Windows*") {Write-Host "Windows"}

