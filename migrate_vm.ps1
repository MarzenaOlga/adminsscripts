﻿#Get-module -name VMware* -ListAvailable
#install-module -Name (Get-module -name VMware* -ListAvailable)
#Import-Module VMware.VumAutomation 


#Install-Module VMware.PowerCLI
Import-Module -Name VMware.VimAutomation.Core 

Connect-VIServer gklab-157-005

$resourcepool = "migr"

if (!(Test-Path C:\Temp\$resourcepool )) {New-Item c:\temp\$resourcepool -ItemType Directory -Force}

#[System.Collections.ArrayList]
$list = (get-vm | where { ($_.ResourcePool -like $resourcepool ) -and ($_.PowerState -eq "PoweredON") } |sort).Name

Foreach ($vmh in $list)
{
[System.Collections.ArrayList]$vm_info = @()
#Write-Host $vmh
$vms = get-vm $vmh
$vm_info+=($vms.Name)
$vm_info+=($vms.Folder.Name)
$vm_info+=($vms.ResourcePool.Name)
$vm_info+=((Get-Datastore -Id $vms.DatastoreIdList).Name)
$vm_info+=((Get-NetworkAdapter $vms).NetworkName)
$vm_info+=((Get-VM $vms).ExtensionData.Config.Files.VmPathName)
Write-Host $vm_info
$vm_info | Out-File c:\temp\$resourcepool\$vmh.txt -Force
Stop-VM $vms.Name -Confirm:$false
Remove-VM $vms.Name -Confirm:$false
}

Disconnect-VIServer $global:DefaultVIServers -Confirm:$false -Force

Connect-VIServer gklab-156-255 -SaveCredentials

[System.Collections.ArrayList]$vmlist = @()
$vmlist = Get-ChildItem C:\Temp\$resourcepool
#Write-Host $vmlist

New-ResourcePool -Name $resourcepool -Location "Haswell"

Get-Folder -Name


Foreach ($vmf in $vmlist){
Write-Host $vmf
[System.Collections.ArrayList]$vm = @()
$vm = Get-Content  c:\temp\$resourcepool\$vmf
if (!(Get-Folder -Name $vm[1])) {New-Folder -Location 'VM' -Name $vm[1]} 
if ($vm[4] -like 'ProcessTeamLab-444') {$Network_Name = 'PT-Lab-444'}
if ($vm[4] -like 'ProcessTeam-1554') {$Network_Name = 'PT-Lab-1554'}
if ($vm[4] -like 'ProcessTeam-sustaining-678') {$Network_Name = 'PT-Lab-678'}
if ($vm[4] -like 'ProcessTeamLab') {$Network_Name = 'PT-Lab-156'}
New-VM -VMFilePath $vm[5] -VMHost 'gklab-156-222.igk.intel.com' -Location $vm[1] -ResourcePool $vm[2] 
Get-VM $vm[0] |Get-NetworkAdapter| Set-NetworkAdapter -NetworkName $Network_Name -Confirm:$false
Set-VM -VM $vm[0] -Version v13 -Confirm:$false 
Start-VM $vm[0]
}


Disconnect-VIServer $global:DefaultVIServers -Confirm:$false -Force