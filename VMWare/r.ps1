﻿Import-Module -Name Scripts\Initialize-PowerCLIEnvironment.ps1
#Import-Module 'ActiveDirectory'

Class Connect
{
    [int] $param
    static [Connect] $instance
    static [Connect] GetInstance()
    {
        if ([Connect]::instance -eq $null)
        {
            [Connect]::instance = [Connect]::new()
        }

        return [Connect]::instance
    }

    Connection()
    {
        Connect-VIServer gklab-157-005
    }
}
$conn = new-object Connect
#$conn.Connection()

Connect-VIServer gklab-157-005
Get-VM $args[0] | Suspend-VM -Confirm:$false