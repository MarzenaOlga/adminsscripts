﻿Import-Module -Name Scripts\Initialize-PowerCLIEnvironment.ps1
Import-Module 'ActiveDirectory'
Connect-VIServer gklab-157-005

[System.Collections.ArrayList]$list = Get-vm | where { $_.PowerState -eq “PoweredOn”} | sort |select Name 

write-host $list

foreach ($a in $list){ if ($a.Name -eq 'gklab-157-005'){write-host $a; $b=$a }}

$list.Remove($b) 
$list | Export-Csv -Path C:\PTScripts\VMWare\Host_list.csv -NoTypeInformation -UseCulture

foreach ($a in $list)
{
Write-Host $a.Name
Get-VM $a.Name | Suspend-VM -Confirm:$false
}
