﻿


$ComputerName = 'gklab-78-187'
$ComputerIP = '10.91.78.186'

#read-host -assecurestring | convertfrom-securestring | out-file .\root_cred.txt
#$pass = get-content .\root_cred.txt | convertto-securestring
#$GC = new-object -typename System.Management.Automation.PSCredential -argumentlist "myusername",$pass

$GC = Get-Credential -Message "input root"



Get-VM $ComputerName  | Invoke-VMScript -GuestCredential $GC "setenforce 0"
Get-VM $ComputerName  | Invoke-VMScript -GuestCredential $GC "rm -rfv /etc/ssh/ssh_host* "
Get-VM $ComputerName  | Invoke-VMScript -GuestCredential $GC "ssh-keygen -q -A -f /etc/ssh/ "
Get-VM $ComputerName  | Invoke-VMScript -GuestCredential $GC "hostnamectl set-hostname '$ComputerName'"
