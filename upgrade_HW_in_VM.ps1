﻿Connect-VIServer gklab-157-005


[System.Collections.ArrayList]$list = (Get-VM | where { ($_.PowerState -eq "PoweredOff") -and ($_.Version -ne "v11" ) } |sort).Name

foreach ($vm in $list)
{
write-host $vm
Start-Job -ScriptBlock { Set-VM -VM $vm -Version v11 -Confirm:$false }
}