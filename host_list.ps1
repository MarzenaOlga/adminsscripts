﻿Import-Module -Name Scripts\Initialize-PowerCLIEnvironment.ps1
Import-Module 'ActiveDirectory'
Connect-VIServer gklab-157-005

$linux=0 
$windows=0
$List = @()
$Akt=0
$dir=""
$ListIP = @()
$Pth = split-path -parent $MyInvocation.MyCommand.Definition
Set-Location -Path $Pth

$List=@()
[System.Collections.ArrayList]$Windows_list = $List
[System.Collections.ArrayList]$Linux_list = $List
[System.Collections.ArrayList]$List_summary = $List
#$Windows_list=@()
#$Linux_list=@()
#$list_summary = @() 


Function IP_List() {
$ListIP = @()
$ip=''
$i=6
$pref_a='10.237.156.'
$pref_b='10.237.157.'
$pref_c='10.91.78.'
$pref_d='10.91.79.'
#$i=1
while ($ip -ne '10.237.156.255') { $ip=$pref_a+$i;$i++;$ListIP += $ip}
$i=0
while ($ip -ne '10.237.157.254') { $ip=$pref_b+$i;$i++;$ListIP += $ip}
$i=6
while ($ip -ne '10.91.78.255') { $ip=$pref_c+$i;$i++;$ListIP += $ip}
$i=0
while ($ip -ne '10.91.79.254') { $ip=$pref_d+$i;$i++;$ListIP += $ip}
return $ListIP 
}


#$id_ip = 10.91.78.234
#10.237.156.34

function Host_name([string]$id_ip) {

#$id_ip = '10.91.78.1'

#$id_ip = '10.237.156.113'

$test2=$id_ip.Substring(0,5)

if ($test2 -eq '10.91'){
$g3=$id_ip.substring(6,2)
$g4=$id_ip.substring(9)}

if ($test2 -eq '10.23'){
$g3=$id_ip.substring(7,3)
$g4=$id_ip.substring(11)}

$g0='gklab'
if ($g4.Length -eq 1) {$g4 = '00{0}' -f $g4}
if ($g4.Length -eq 2) {$g4 = '0{0}' -f $g4}
$VMname='{0}-{1}-{2}' -f $g0,$g3,$g4

return $VMname
Write-Host $VMname
}


$a = IP_List
write-host $a
write-host #########################################################################################################################################################
foreach ($vm in $a){ 
    $test = Test-Connection $vm -Count 1 -quiet 
    if ($test) {
        write-host $vm,' '  -nonewline
        $ComputerName = Host_name($vm)
        write-host $ComputerName,' ' -nonewline

        # OS choice
        $tcpClient22 = New-Object System.Net.Sockets.TCPClient
        $tcpClient3389 = New-Object System.Net.Sockets.TCPClient
        try { $tcpClient22.Connect($vm,22) # | out-null 
        }
        #sprawdzamy czy ma otwarty port 22 - domyslnie unix
        catch { }
        if ($tcpClient22.Connected) {Write-Host -foregroundcolor green " Linux"; $Os = 0; $Linux_list +=$ComputerName }
        try {$tcpClient3389.Connect($vm,3389) #| out-null 
        }
        #sprawdzamy czy ma otwarty port 3389 - zakladamy windows
        catch { }
        if ($tcpClient3389.Connected) {Write-Host -foregroundcolor yellow " Windows"; $Os = 1; #$Windows_list +=$ComputerName 
        }
        if ($Os -eq 1){
         if (Test-Path -Path \\$ComputerName\c$\tools ) { $Windows_list +=$ComputerName }

        }

       
        
        }  
}

$Linux_list | Out-File 'c:\temp\new_linux_list.txt'
$Windows_list | Out-File 'c:\temp\new_windows_list_tools.txt'




