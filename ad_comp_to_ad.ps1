﻿#Import-Module 'ActiveDirectory'
Import-Module -Name Scripts\Initialize-PowerCLIEnvironment.ps1
Import-Module 'ActiveDirectory'
Connect-VIServer gklab-157-005


#Import-Module 'DnsClient'

$ComputerName = read-host('Hostname')

#############################################################################################################
#create local root credential
$root_name='root'
$sec_root_pwd = '[d0n0tchang3]' | ConvertTo-SecureString -AsPlainText -Force 
$GC = New-Object System.Management.Automation.PSCredential -ArgumentList $root_name, $sec_root_pwd
#############################################################################################################

Get-VM gklab-79-000  | Invoke-VMScript -GuestCredential $GC "/root/rem_acc.sh $ComputerName" -ScriptType Bash


#$ComputerName = 'gklab-157-142'
#$ComputerIP = '10.91.78.146'
New-ADComputer -Name $ComputerName -SAMAccountName $ComputerName -ManagedBy "ITP Process Team Admins" -Description 'owner:rkupniex' -DNSHostName $ComputerName.ger.corp.intel.com -Path "OU=Web,OU=Servers,OU=IGK,OU=Customer Labs,OU=Engineering Computing,OU=Resources,DC=ger,DC=corp,DC=intel,DC=com" -Enabled $True -Location "IGK-GDANSK"
Get-ADComputer $ComputerName
