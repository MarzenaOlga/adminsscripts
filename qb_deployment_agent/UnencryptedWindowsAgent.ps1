﻿# INTEL CONFIDENTIAL
# Copyright 2016 Intel Corporation All Rights Reserved.
# The source code contained or described herein and all documents related to the source code ("Material") are owned by Intel Corporation or its suppliers or licensors. 
# Title to the Material remains with Intel Corporation or its suppliers and licensors. 
# The Material contains trade secrets and proprietary and confidential information of Intel or its suppliers and licensors. 
# The Material is protected by worldwide copyright and trade secret laws and treaty provisions.
# No part of the Material may be used, copied, reproduced, modified, published, uploaded, posted, transmitted, distributed, or disclosed in any way without Intelâ€™s prior express written permission.
# No license under any patent, copyright, trade secret or other intellectual property right is granted to or conferred upon you by disclosure or delivery of the Materials, either expressly, by implication, inducement, estoppel or otherwise. 
# Any license under such intellectual property rights must be express and approved by Intel in writing.

#RUN THIS AS ADMIN 

Get-Disk | Where partitionstyle -eq 'raw' | Initialize-Disk -PartitionStyle GPT -PassThru | New-Partition -AssignDriveLetter -UseMaximumSize | Format-Volume -FileSystem NTFS -NewFileSystemLabel "Encrypted" -Confirm:$false
robocopy \\storage.igk.intel.com\stuff\qba2015\qba1 D:\qba1 /E 
icacls D:\qba1 /grant '"GER\gkblditp":(OI)(CI)F'
#cmd /c D:\qba1\bin\agent.bat install
#$gkblditpPasss = read-host -Prompt "gkblditp password for qba1 service"  
#sc.exe config "QuickBuild Build Agent 1" obj= "gkblditp@intel.com" password= "$gkblditpPasss" 
#sasv quickbuild*
New-Service -Name "QuickBuild Build Agent 1" -BinaryPathName "D:\qba1\bin\wrapper-windows-x86-64.exe -s D:\qba1\conf\wrapper.conf" -StartupType Automatic -Credential "gkblditp@intel.com" -Description "QuickBuild Build Agent 1" -DisplayName "QuickBuild Build Agent 1" 
Start-Service -Name "QuickBuild Build Agent 1"

#(Get-WmiObject -Class Win32_Service -Filter "Name='QuickBuild Build Agent 1'").delete()
