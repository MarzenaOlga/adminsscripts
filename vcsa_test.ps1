﻿


###################################################################################################
if (Get-module -name VMware* -ListAvailable)
{ Import-Module VMware.PowerCLI }
else 
{
Find-Module -Name VMware.PowerCLI 
Install-Module VMware.PowerCLI -Confirm:$false
}
###################################################################################################


###################################################################################################
$User = 'Administrator@vsphere.local'
[securestring]$Password = ConvertTo-SecureString '[D0n0tchang3]' -AsPlainText -Force
[pscredential]$Cred = New-Object System.Management.Automation.PSCredential ($User, $Password)
###################################################################################################

###################################################################################################
Set-PowerCLIConfiguration -InvalidCertificateAction Ignore -Confirm:$false
Connect-VIServer 180.30.1.5 -Credential $Cred
###################################################################################################

###################################################################################################
Function Select-Template {
$tmpl_list = Get-ResourcePool -Name "Templates" | Get-VM | Sort-Object 
$a=$tmpl_list.Length
  
For ($i=0; $i -lt $tmpl_list.Length; $i++){
 Write-Host  $i," - ", $tmpl_list[$i]
 }

$Name_Source_VM = ''
$indx=-1

while ($indx -lt 0){
try{
    $indx = Read-Host -Prompt  'Select template '
    if ($indx -eq '') {$indx='wrong'} 
    [int]$indx = [convert]::ToInt32($indx)   
    if (($indx -lt 0)  -or ($indx -gt $a) ){$indx='minus'} 
    [int]$indx = [convert]::ToInt32($indx)
    }
catch {Write-Host "Select template 0 - $($a-1) "; $indx=-1 }
}
return $tmpl_list[$indx]
#Write-Host $Name_Source_VM
}
###################################################################################################
$Name_Source_VM=Select-Template
$Name_Source_VM=$Name_Source_VM.Name
###################################################################################################
$san = Get-Cluster 'ND-ESXI' | Get-Datastore -Name "SSD*" | sort -Property FreeSpaceGB -Descending 
$Name_Datastore = $san.Name
###################################################################################################

###################################################################################################
$list=@(1..4)
$vms=@()

foreach ($li in $list)
{
$vms+='test_'+$li
}
###################################################################################################

###################################################################################################
Function Start-VMS-Lab
{
foreach ($vm in $vms)
{
write-host $vm
New-VM -Name $vm -VM $Name_Source_VM -Datastore $Name_Datastore -ResourcePool 'Lab'
}

foreach ($vm in $vms)
{
Start-VM $vm
}
}

Function Remove-VMS-Lab
{
foreach ($vm in $vms)
    {
    write-host $vm
    Stop-VM $vm -Confirm:$false 
    }

foreach ($vm in $vms)
    {
    Remove-VM $vm -DeletePermanently -Confirm:$false
    }
}

###################################################################################################

Start-VMS-Lab

Remove-VMS-Lab




