﻿
# Author Marzena Kupniewska
# The script should be run in 64-bit PowerCli, preferably as Administrator, due to the increase in the buffer in which the zip package is created
# The script creates zip packages on the storage.igk.intel.com server and before running the script you need to check if there is write access
# Script as a model source of toola used the machine gklab-156-017 but when create the next packages recommend using the local directory, for example c:\tools where we put a specific package that we want to save
# The script checks whether the subdirectory of the compressed package are files or directories and depending on the result creates one package (files) or packages corresponding to the names of subdirectories, ignores the symlinks "latest"
# I have not come up with a better idea of ​​distinguishing whether there is one program or several programs in separate catalogs in the catalog

# Skrypt nalezy uruchamiac w 64-bitowym PowerCli najlepiej jako Administrator ze wzgledu na powiekszanie bufora w ktorym jest tworzona paczka zip
# Skrypt tworzy paczki zip na serwerze storage.igk.intel.com i przed uruchomieniem skryptu nalezy sprawdzic czy sie ma tam dostep do zapisu
# Skrytp jako wzorcowe zrodlo tooli uzyl maszyny gklab-156-017 ale przy dogrywaniu nastepnych paczek zalecam uzywanie katalogu lokalnego np c:\tools gdzie umieszczamy konkretna paczke ktora chcemy dograc
# Skrypt sprawdza czy w podkatalogu z kompresowana paczka sa pliki czy same katalogi i w zaleznosci od rezultatu tworzy jedna paczke(pliki) lub paczki odpowiadajace nazwom podkatalogow, ignoruje symlinki "latest"
# Nie wpadlam na lepszy pomysl rozrozniania czy w katalogu jest jeden program czy pare programow w osobnych katalogach


Set-Item WSMan:\localhost\Shell\MaxMemoryPerShellMB 30000 
Set-Item WSMan:\localhost\Plugin\Microsoft.PowerShell\Quotas\MaxMemoryPerShellMB 30000 
Restart-Service WinRM


$path = 'C:\tools'
#$path = '\\gklab-156-017\c$\tools\'
$pathd = '\\storage\salt_tools\windows-tools\zipped-tools\'
$list = Get-ChildItem $path


$res = Get-ChildItem $pathd
if (!$res) {write-host "Access denied";Break}

foreach ($li in $list){
    $fileTest=0
    write-host $li
    $path_c = $path + '\' + $li
    $path_d = $pathd + '\' + $li
    Write-Host $path_c
    $list_c = Get-ChildItem $path_c
    foreach ($lic in $list_c) { 
        if ($lic.GetType().Name -eq 'FileInfo' ) {$fileTest=1 }
    } 
    if ($fileTest -eq 1) {
        $dest = '{0}\{1}.zip' -f $path_d,$li
        $src = $path_c
        if (!(Test-Path $path_d)) { New-Item -ItemType directory -Path  $path_d } 
        Compress-Archive -Path $src -DestinationPath $dest -Force
        }

    if ($fileTest -eq 0) {
        foreach ($lic in $list_c) {
            $dest = '{0}\{1}.zip' -f $path_d,$lic
            $src = '{0}\{1}' -f $path_c,$lic
            if ($lic -notlike 'latest'){ 
            if (!(Test-Path $path_d)) { New-Item -ItemType directory -Path  $path_d } 
            Compress-Archive -Path $src -DestinationPath $dest -Force
            Write-Host $src $dest $lic
            }
        }
    }
}