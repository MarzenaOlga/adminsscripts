﻿Import-Module -Name Scripts\Initialize-PowerCLIEnvironment.ps1
Import-Module 'ActiveDirectory'
Connect-VIServer gklab-157-005



#############################################################################################################
$KeyFile = "c:\passwordstore\AES.key"
$key = Get-Content $KeyFile
#############################################################################################################
$RootUser = "root"
$RootPasswordFile = "c:\passwordstore\Root_Pass.txt"
$GC = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $RootUser, (Get-Content $RootPasswordFile | ConvertTo-SecureString -Key $key)
#############################################################################################################
$AdmUser = "administrator"
$AdmPasswordFile = "c:\passwordstore\Adm_Pass.txt"
$Credential = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $AdmUser, (Get-Content $AdmPasswordFile | ConvertTo-SecureString -Key $key)
#############################################################################################################
$GKBUser = "gkblditp"
$GKBPasswordFile = "c:\passwordstore\GKB_Pass.txt"
$gkblditp_cred = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $GKBUser, (Get-Content $GKBPasswordFile | ConvertTo-SecureString -Key $key)
#############################################################################################################




#$ComputerName='gklab-156-075'

Write-host ("Hostname = ") -NoNewline
$ComputerName = Read-Host


Get-VM gklab-79-000  | Invoke-VMScript -GuestCredential $GC "/root/rem_acc.sh $ComputerName" -ScriptType Bash
#add new computer account
New-ADComputer -Name $ComputerName -SAMAccountName $ComputerName -ManagedBy "ITP Process Team Admins" -Description 'owner:rkupniex' -DNSHostName $ComputerName.ger.corp.intel.com -Path "OU=Web,OU=Servers,OU=IGK,OU=Customer Labs,OU=Engineering Computing,OU=Resources,DC=ger,DC=corp,DC=intel,DC=com" -Enabled $True -Location "IGK-GDANSK"
#check ad computer account
Get-ADComputer $ComputerName 
