﻿$IPArray=New-Object System.Collections.ArrayList
#$IPArray= Get-Content C:\Tools\Windows_dns_list.txt
#$IPArray= Get-Content C:\Temp\ilab\ilab_hosts_list.txt
$linux=0 
$windows=0
$List = @()
$Act=0
$secpasswd = ConvertTo-SecureString "ldap" -AsPlainText -Force
$mycreds = New-Object System.Management.Automation.PSCredential ("ldap", $secpasswd)
$serverAddress = "https://quickbuild.igk.intel.com:8810"
[System.Collections.ArrayList]$port = $List
[System.Collections.ArrayList]$Windows_list = $List
[System.Collections.ArrayList]$Linux_list = $List
[System.Collections.ArrayList]$Act_list = $List
[System.Collections.ArrayList]$SRV_lista = $List
$port = (':8841',':8842',':8843',':8844',':8845',':8846')
$IPArray= Get-Content C:\Temp\restart.txt
#$IPArray=('gklab-156-152','gklab-156-186','gklab-156-057')

Clear-Host
Write-Host "Host List:"
write-host $IPArray
#tu mamy adresy IP
  For ($i=0; $i -lt $IPArray.Length; $i++)
  
    {
     $ip=$IPArray[$i] 
     write-host "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
     Write-Host  'Host '$ip 
     $test=Test-Connection $ip -count 1 -quiet
     #sprawdzamy ICMP czy jest host
     if (!$test) { Write-Host ' Not responding'}
     if ($test)
        {
        $tcpClient22 = New-Object System.Net.Sockets.TCPClient
        $tcpClient3389 = New-Object System.Net.Sockets.TCPClient
        try { $tcpClient22.Connect($ip,22)>$null }
        #sprawdzamy czy ma otwarty port 22 - domyslnie unix
        catch { }

        if ($tcpClient22.Connected) {$linux++; Write-Host -foregroundcolor green " Linux";$Linux_list.Add($ip)>null}
        if (!$tcpClient22.Connected) 
            {
                 try { $tcpClient3389.Connect($ip,3389)>$null }
                 #sprawdzamy czy ma otwarty port 3389 i nieotwarty 22 - zakladamy windows
                 catch { }
                 if ($tcpClient3389.Connected){
                    $licznik=0
                    # ten tray catch jest po to gdyz  czasami po restarcie zdalny dostep via powershell jeszcze "spi" 
                    try {Get-Service  -ComputerName $ip -Name "Quick*" }
                        catch {}
                    $test = (Get-Service  -ComputerName $ip -Name "Quick*" ).count
                    For ($j=0; $j -lt $test; $j++){
                        $Remote = $ip
                        $Remote += $port[$j]
                        $buildAgent = $Remote
                        $response = Invoke-RestMethod -Uri $serverAddress/rest/buildagents/$buildAgent/running_steps -Credential $mycreds
                        $licznik += ($response.list.'com.pmease.quickbuild.RunningStepInfo' | measure).Count
                        }
                    write-host "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
                    write-host "Running steps = " $licznik  
                    write-host "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
                    #licznik liczy ilosc stepow (suma) dla wszystkich agentow (do 6go)
                    if ($licznik -gt 0) { Write-Host "Builds running & Stop Script !!!" }
                    if ($licznik -eq 0) {
                        Write-host ("******* Stop QBA & Restart & Start QBA *******")
                        $testQB = (Get-Service  -ComputerName $ip -Name "Quick*").count
                        Write-host "Services = " $testQB
                        if ($testQB -eq 1) {
                        $srv = (Get-Service  -ComputerName $ip -Name "Quick*").ToString()
                        write-host "Stopping " $srv
                        Get-Service -ComputerName $ip -Name $srv | Set-Service -Status Stopped
                        }

                        if ($testQB -gt 1){
                        $SRV_lista = (Get-Service  -ComputerName $ip -Name "Quick*" )
                        For ($k=0; $k -lt $SRV_lista.Count; $k++){
                            Write-Host "Stopping " $SRV_lista[$k] 
                            $srv=($SRV_lista[$k]).ToString()                        
                            Get-Service -ComputerName $ip -Name $srv | Set-Service -Status Stopped }}


                        Write-host "Restarting Host" 
                        restart-computer -Wait -Force $ip 
                        #Start-Sleep -s 60

                        #sprawdzamy i odblokowujemy szyfrowany dysk
                        Write-host "Unlock Disk"
                        try { $chkbit = manage-bde -status d: -computername $ip  | Select-String -Pattern 'Lock Status' }
                            catch {}
                        #write-host $chkbit
                        if ($chkbit -like '*unlocked*'){ write-host "Disc D: Unlocked"}
                        else { 
                        write-host "Disc D: Locked -> Unlock"
                        #Start-Sleep -s 5
                        Invoke-Command -Computername $ip  -ScriptBlock {                             
                            $SecureString = ConvertTo-SecureString "[d0n0tchang3]" -AsPlainText -Force
                            Unlock-BitLocker -MountPoint "D:" -Password $SecureString }  }
                    
                        #sprawdzamy servisy QuickBuild i uruchamiamy je
                        Write-host "Starting Service(s) "
                        #ten try catch jest po to jak na poczatku skryptu. Jego brak wywoluje bledy przy startowaniu pierwszego servisu.
                        try {Get-Service  -ComputerName $ip -Name "Quick*" }
                        catch {}
                        write-host "Servicesto start = " $testQB
                        if ($testQB -eq 1) {
                            Write-host $ip " Starting " $srv
                            Get-Service -ComputerName $ip -Name $srv | Start-Service }
                        if ($testQB -gt 1){
                            For ($l=0; $l -lt $SRV_lista.Count; $l++){
                                $srv=($SRV_lista[$l]).ToString() 
                                Write-host $ip " Starting " $srv                       
                                Get-Service -ComputerName $ip -Name $srv | Start-Service }}

                        $Act_list.Add($ip)>$null
                        $Act++
                    }
                 }                    
            }
        }
    } 

Write-Host ""
Write-Host -foregroundcolor yellow '--------------------------------------------------' 
Write-Host -foregroundcolor red 'Zaktualizowano:' $Act
Write-Host -foregroundcolor yellow  'Zaktualizowano:' $Act_list
Write-Host -foregroundcolor yellow '--------------------------------------------------' 
$Act_list | Out-File c:\temp\QBA_reStart_list.txt
