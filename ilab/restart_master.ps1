﻿$IPArray=New-Object System.Collections.ArrayList
$linux=0 
$windows=0
$List = @()
$Akt=0
$Pth = split-path -parent $MyInvocation.MyCommand.Definition
$log=$Pth,"ilab_hosts_list.txt" -join "\"
$log4=$Pth,"restart_log.txt" -join "\"
$IPArray= Get-Content $log
$HoList = @("host","Uptime")
[System.Collections.ArrayList]$Windows_list = $List
[System.Collections.ArrayList]$Linux_list = $List
[System.Collections.ArrayList]$Res_list = $List

Clear-Host
Write-Host ""
Write-Host $log
Get-Date | Out-File $log4
#tu mamy adresy IP
  For ($i=0; $i -lt $IPArray.Length; $i++)
  
    {
     $ip=$IPArray[$i] 
     write-Host " "
     Write-Host -NonewLine 'Check '$ip
     $ho=$ip+".igk.intel.com" 
     $test=Test-Connection $ho -count 1 -quiet
     #sprawdzamy ICMP czy jest host
     
     if (!$test) { Write-Host -NonewLine ' Not responding'}
     if ($test)
        {
        $tcpClient22 = New-Object System.Net.Sockets.TCPClient
        $tcpClient3389 = New-Object System.Net.Sockets.TCPClient
        $ho2=$ip+".igk.intel.com"
        try { $tcpClient22.Connect($ho2,22)| Out-Null }
        #sprawdzamy czy ma otwarty port 22 - domyslnie unix
        catch { }

        if ($tcpClient22.Connected) {$linux++; Write-Host -NonewLine -foregroundcolor green " Linux";$Linux_list.Add($ho2)| Out-Null}
        if (!$tcpClient22.Connected) 
            {
                 try { $tcpClient3389.Connect($ip,3389)| Out-Nul}
                 #sprawdzamy czy ma otwarty port 3389 i nieotwarty 22 - zakladamy windows
                 catch { }
                 if ($tcpClient3389.Connected)
                    {
                    $ho=$ip+".ger.corp.intel.com"
                    $windows++; Write-Host -NonewLine -foregroundcolor yellow  " Windows ";$Windows_list.Add($ho)| Out-Null;
                    $up=Invoke-Command -Computername $ho  -ScriptBlock{ (get-date) – (gcim Win32_OperatingSystem).LastBootUpTime}
                    if ($up.TotalDays -gt 0) {
                        Write-Host -NonewLine -foregroundcolor red  " Uptime =" $up.TotalDays " ";  
                        $Res_list.Add($ho)| Out-Null
                        Start-Sleep -Seconds 3  
                        #Write-Host $ho $log4  
                        $ScriptBlock = [scriptblock]::Create(“ $Pth\restart_slave_script.ps1 $ho $log4 ”)
                        #Start-Job -ScriptBlock $ScriptBlock -Verbose
                        Start-Job -filepath $Pth\restart_slave_script_ilab.ps1 -ArgumentList $ho,$log4
                        #Start-Job -filepath $Pth\t1.ps1 -ArgumentList $ho,$log4
                        $HoList += @($ho, $up.TotalDays)
                         }
                    else {Write-Host -NonewLine -foregroundcolor blue  " Uptime =" $up.TotalDays}
                    }
            }
        }
    } 

Write-Host ""
#Write-Host $HoList
Write-Host -foregroundcolor yellow '--------------------------------------------------' 
Write-Host -foregroundcolor yellow 'Linux:' $linux -NoNewLine
Write-Host -foregroundcolor yellow ' Windows:' $Windows
#Write-Host -foregroundcolor yellow ' Uptime:' $Res_list
Write-Host -foregroundcolor yellow '--------------------------------------------------' 

$log1=$Pth,"Windows_DNS_list.txt" -join "\"
$log2=$Pth,"Linux_DNS_list.txt" -join "\"
$log3=$Pth,"restart_host_list.txt" -join "\"
$log5=$Pth,"uptime_host_list.txt" -join "\"

$HostsList | Out-File $log5
$Windows_list | Out-File $log1
$Res_list | Out-File $log3
$Linux_list | Out-File $log2
