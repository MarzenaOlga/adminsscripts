﻿$IPArray=New-Object System.Collections.ArrayList
$linux=0 
$windows=0
$List = @()
$Act=0

$Pth = split-path -parent $MyInvocation.MyCommand.Definition
$log4=$Pth,"restart_log.txt" -join "\"


$secpasswd = ConvertTo-SecureString "ldap" -AsPlainText -Force
$mycreds = New-Object System.Management.Automation.PSCredential ("ldap", $secpasswd)
#$serverAddress = "https://quickbuild.igk.intel.com:8810"
$serverAddress = "https://pt-irst.intel.com:8810" 
[System.Collections.ArrayList]$port = $List
$port = (':8841',':8842',':8843',':8844',':8845',':8846')
$error_num=0
     $ip = $args[0]
     $log4 = $args[1]
     #$ip=$IPArray[$i] 
     #write-host "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
     #Write-Host  'Host '$ip 
     $test=Test-Connection $ip -count 1 -quiet
     #sprawdzamy ICMP czy jest host
     if (!$test) { $error_num=$error_num+1}
     if ($test)
        {
        $tcpClient22 = New-Object System.Net.Sockets.TCPClient
        $tcpClient3389 = New-Object System.Net.Sockets.TCPClient
        try { $tcpClient22.Connect($ip,22)>$null }
        #sprawdzamy czy ma otwarty port 22 - domyslnie unix
        catch { }

        if ($tcpClient22.Connected) {$error_num=$error_num+2}
        if (!$tcpClient22.Connected) 
            {
                 try { $tcpClient3389.Connect($ip,3389)>$null }
                 #sprawdzamy czy ma otwarty port 3389 i nieotwarty 22 - zakladamy windows
                 catch { $error_num=$error_num+4 }
                 if ($tcpClient3389.Connected){
                    $licznik=0
                    # ten tray catch jest po to gdyz  czasami po restarcie zdalny dostep via powershell jeszcze "spi" 
                    try {Get-Service  -ComputerName $ip -Name "Quick*" }
                        catch {$error_num=$error_num+8}
                    $test = (Get-Service  -ComputerName $ip -Name "Quick*" ).count
                    if ($test -eq 0) {$error_num=$error_num+32}
                    For ($j=0; $j -lt $test; $j++){
                        $Remote = $ip
                        $Remote += $port[$j]
                        $buildAgent = $Remote
                        $response = Invoke-RestMethod -Uri $serverAddress/rest/buildagents/$buildAgent/running_steps -Credential $mycreds
                        $licznik += ($response.list.'com.pmease.quickbuild.RunningStepInfo' | measure).Count
                        }
                    #*********************************************************************
                    #licznik liczy ilosc stepow (suma) dla wszystkich agentow (do 6go)
                    if ($licznik -gt 0) { $error_num=$error_num+16}
                    if ($licznik -eq 0) {
                        $testQB = (Get-Service  -ComputerName $ip -Name "Quick*").count
                        if ($testQB -eq 1) {
                        $srv = (Get-Service  -ComputerName $ip -Name "Quick*").ToString()
                        write-host "Stopping " $srv
                        Get-Service -ComputerName $ip -Name $srv | Set-Service -Status Stopped
                        }

                        if ($testQB -gt 1){
                        $SRV_lista = (Get-Service  -ComputerName $ip -Name "Quick*" )
                        For ($k=0; $k -lt $SRV_lista.Count; $k++){
                            #Write-Host "Stopping " $SRV_lista[$k] 
                            $srv=($SRV_lista[$k]).ToString()                        
                            Get-Service -ComputerName $ip -Name $srv | Set-Service -Status Stopped }}
                        #***********************************************************************************
                        restart-computer -Wait -Force $ip 
                        #***********************************************************************************
                        #sprawdzamy i odblokowujemy szyfrowany dysk
                        try { $chkbit = manage-bde -status d: -computername $ip  | Select-String -Pattern 'Lock Status' }
                            catch {}

                        if ($chkbit -like '*unlocked*'){ write-host "Disc D: Unlocked"}
                        else { 
                        Invoke-Command -Computername $ip  -ScriptBlock {                             
                            $SecureString = ConvertTo-SecureString "[d0n0tchang3]" -AsPlainText -Force
                            Unlock-BitLocker -MountPoint "D:" -Password $SecureString }  }
                        #************************************************************************************
                        #sprawdzamy servisy QuickBuild i uruchamiamy je
                        #ten try catch jest po to jak na poczatku skryptu. Jego brak wywoluje bledy przy startowaniu pierwszego servisu.
                        try {Get-Service  -ComputerName $ip -Name "Quick*" }
                        catch {}
                        if ($testQB -eq 1) {Get-Service -ComputerName $ip -Name $srv | Start-Service }
                        if ($testQB -gt 1){
                            For ($l=0; $l -lt $SRV_lista.Count; $l++){
                                $srv=($SRV_lista[$l]).ToString() 
                                Get-Service -ComputerName $ip -Name $srv | Start-Service }}
                        #***************************************************************************
                        $ip+" - "+ $error_num | Add-Content $log4 }
                    }
                 }                    
            }
        #}
    
