﻿#Import-Module  -Name SqlServer
import-module sqlps
#Backup-SqlDatabase
#GKPT01SQL\CLUSTER
#Get-ChildItem "SQLSERVER:\SQL\GKPT01SQL\CLUSTER\Databases" | Backup-SqlDatabase -BackupFile 

$now = Get-Date
$day = $now.DayOfYear
$time = $now.TimeOfDay
$time=$time -Replace(":",".")
Set-Location "SQLSERVER:\SQL\GKPT01SQL\CLUSTER\Databases"
ForEach($database in (Get-ChildItem)) {
$dbName = $database.Name
Backup-SqlDatabase -CompressionOption On -Database $dbName -BackupFile "\\10.237.156.250\databasebackup\$dbName.$day.$time.bak"
#Backup-SqlDatabase -CompressionOption On -Database $dbName -BackupFile "\\gksfiler\nfs\igk\disks\igk_pt_backups\$dbName.$day.bak"
}