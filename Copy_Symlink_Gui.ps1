﻿$HostArray=New-Object System.Collections.ArrayList

#$pth= "c$\tools"
$windows=0
$List = @()
$Akt=0
$dir="test"
$error_1=0

[System.Collections.ArrayList]$Windows_list = $List

Add-Type -AssemblyName System.Windows.Forms
$form = New-Object Windows.Forms.Form
$form.Text="Copy Folder Script"
$form.Size = New-Object Drawing.Size @(600,600)
$form.StartPosition = "CenterScreen"

$objTypeCheckbox = New-Object System.Windows.Forms.Checkbox 
$objTypeCheckbox.Location = New-Object System.Drawing.Size(250,129) 
$objTypeCheckbox.Size = New-Object System.Drawing.Size(100,15)
$objTypeCheckbox.Text = "Create Symlink"
$objTypeCheckbox.TabIndex = 0
$Form.Controls.Add($objTypeCheckbox)



$Label_1 = New-Object System.Windows.Forms.Label
$Label_1.Text="Select folder to copy"
$Label_1.Location = New-Object System.Drawing.Size(440,25)
$Label_1.AutoSize = $True

$Label_2 = New-Object System.Windows.Forms.Label
$Label_2.Text="Select list of hosts (file)"
$Label_2.Location = New-Object System.Drawing.Size(440,65)
$Label_2.AutoSize = $True

$Label_3 = New-Object System.Windows.Forms.Label
$Label_3.Text="Modify the target path"
$Label_3.Location = New-Object System.Drawing.Size(440,105)
$Label_3.AutoSize = $True


$Runbtn = New-Object System.Windows.Forms.Button
$Runbtn.add_click({Run-Copy-Action })
$Runbtn.Text = "Run"
$Runbtn.Location = New-Object System.Drawing.Size(260,525)

$Browsebtn = New-Object System.Windows.Forms.Button
$Browsebtn.add_click({Select-FolderDialog   })
$Browsebtn.Text = "Source"
$Browsebtn.Location = New-Object System.Drawing.Size(20,20)

$Openbtn = New-Object System.Windows.Forms.Button
$Openbtn.add_click({Get-FileName  })
$Openbtn.Text = "Hosts"
$Openbtn.Location = New-Object System.Drawing.Size(20,60)

$objLabel = New-Object System.Windows.Forms.Label
$objLabel.Location = New-Object System.Drawing.Size(120,65) 
$objLabel.Size = New-Object System.Drawing.Size(280,20) 
$objLabel.Text = $folder

Function Get-FileName($initialDirectory)
{
    [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null
    
    $OpenFileDialog = New-Object System.Windows.Forms.OpenFileDialog
    $OpenFileDialog.initialDirectory = $initialDirectory
    $OpenFileDialog.filter = "all (*.*)| *.*"
    $OpenFileDialog.ShowDialog() | Out-Null
    $OpenFileDialog.filename
    $outputBox2.text=$OpenFileDialog.FileName 
    $nazwa=$OpenFileDialog.filename
    $HostArray= Get-Content $nazwa
    $outputBox.text=$Hostarray
    #Write-Host $HostArray
}


Function Select-FolderDialog
    {
     param([string]$Description="Select Folder",[string]$RootFolder="Desktop"  )
     #Set-Location -Path C:\Tools   
     [System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null     

     $objForm = New-Object System.Windows.Forms.FolderBrowserDialog
     $objForm.Rootfolder = $RootFolder
     $objForm.Description = $Description
     $Show = $objForm.ShowDialog()
     If ($Show -eq "OK")
     {
         $outputBox1.text=$objForm.SelectedPath
         $dest=$objForm.SelectedPath
         $src_dir=$dest
         $dest=$dest.Substring(2,$dest.Length-2)
         $dest="c$"+$dest
         $outputBox3.text=$dest
         Return $objForm.SelectedPath
     }
     Else
     {
        $error_1=1
     }
    }

Function Run-Copy-Action 
    {
        #tu mamy liste hostow
        $outputBox.AppendText( "`r`n")
        $outputBox.AppendText( "Run Script... `r`n")
        $nazwa=$OutputBox2.Text
        #write-host $nazwa
        $HostArray= Get-Content $nazwa
        $dest=$OutputBox3.Text
        $src_dir=$OutputBox1.Text
        $sciezka = $src_dir
        while ($sciezka.Substring($sciezka.Length-1) -ne "\"){$sciezka = $sciezka.TrimEnd($sciezka.Substring($sciezka.Length-1))}
        $sciezka = $sciezka.TrimEnd("\")
        $link=$sciezka,"latest" -join "\"
        $rm_link="rmdir",$link -join " "
        $Akt=0
        $Windows_list =@()
                
        #For ($i=0; $i -lt $HostArray.Length; $i++)
        Foreach($ip in $HostArray)
            {
                #$ip=$HostArray[$i] 
                $ip = $ip.Trim()
                #write-host $ip
                $outputBox.AppendText( "Check "+ $ip+ "`r`n" )
                $test=Test-Connection $ip -count 1 -quiet
                #sprawdzamy ICMP czy jest host
                if (!$test) { $outputBox.AppendText( "Not responding `r`n")}
                if ($test)
                    { 
                        $windows++; $outputBox.AppendText(  "Windows " + $ip+ "`r`n" ) | Out-Null;
                        if (Test-Path \\$ip\$dest\ ){ Remove-Item \\$ip\$dest\ -Recurse | Out-Null }
                        $outputBox.AppendText("Copy from "+$src_dir+" to \\" +$ip+"\"+$dest+ "`r`n")
                        mkdir \\$ip\$dest | Out-Null
                        copy-Item $src_dir\* \\$ip\$dest\ -Recurse | Out-Null
                        
                        If ($objTypeCheckbox.Checked -eq $true)
                        {
                            try{ $script_block_r = [scriptblock]::Create(“ & cmd.exe /c '$rm_link ' ”) }
                            catch{}
                            Invoke-Command -Computername $ip  -ScriptBlock  $script_block_r
                            $script_block = [scriptblock]::Create(“ & cmd.exe /c 'mklink /d $link $src_dir ' ”)
                            Invoke-Command -Computername $ip  -ScriptBlock  $script_block
                            $outputBox.AppendText("Symbolic link created for "+$link+" <<===>> " +$src_dir+ "`r`n")
                        }
                        $Akt++

                    }
            } 
        $outputBox.AppendText( "Script completed `r`n")
        $outputBox.AppendText( $Akt )
        $outputBox.AppendText( " Updated hosts: `r`n")
        $outputBox.AppendText(  $Windows_list + "`r`n")

    }


$outputBox = New-Object System.Windows.Forms.TextBox 
$outputBox.Location = New-Object System.Drawing.Size(10,150) 
$outputBox.Size = New-Object System.Drawing.Size(565,350) 
$outputBox.MultiLine = $True 
$outputBox.ScrollBars = "Vertical" 
$outputBox.ReadOnly = $True

$outputBox1 = New-Object System.Windows.Forms.TextBox 
$outputBox1.Location = New-Object System.Drawing.Size(120,22) 
$outputBox1.Size = New-Object System.Drawing.Size(300,20) 
$outputBox1.MultiLine = $False 
#$outputBox1.ScrollBars = "Vertical" 
$outputBox1.ReadOnly = $True

$outputBox2 = New-Object System.Windows.Forms.TextBox 
$outputBox2.Location = New-Object System.Drawing.Size(120,62) 
$outputBox2.Size = New-Object System.Drawing.Size(300,20) 
$outputBox2.MultiLine = $False 
#$outputBox2.ScrollBars = "Vertical" 
$outputBox2.ReadOnly = $True

$outputBox3 = New-Object System.Windows.Forms.TextBox 
$outputBox3.Location = New-Object System.Drawing.Size(120,102) 
$outputBox3.Size = New-Object System.Drawing.Size(300,20) 
$outputBox3.MultiLine = $False 
#$outputBox3.ScrollBars = "Vertical"
#$outputBox3.ReadOnly = $True


$Form.Controls.Add($Label_3)
$Form.Controls.Add($Label_2)
$Form.Controls.Add($Label_1)
$Form.Controls.Add($outputBox3)
$Form.Controls.Add($outputBox2)
$Form.Controls.Add($outputBox1)
$Form.Controls.Add($outputBox) 
$form.Controls.Add($objLabel) 
$form.Controls.Add($Browsebtn)
$form.Controls.Add($Openbtn)
$form.Controls.Add($Runbtn)
$drc = $form.ShowDialog()